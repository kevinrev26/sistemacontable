/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.negocio;

import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import sv.edu.ues.fia.persistencia.PlanDeCuentas;
import sv.edu.ues.fia.servicios.ServicioBalanza;
import sv.edu.ues.fia.servicios.ServicioPlanDeCuentas;

/**
 *
 * @author Kevin
 */
public class EstadoFinanciero {
        private final EntityManagerFactory emf;
        private final ServicioBalanza service;
        private final ServicioPlanDeCuentas servicioCuentas;
        
        public EstadoFinanciero(){
            emf= Persistence.createEntityManagerFactory("base");
            service = new ServicioBalanza(emf.createEntityManager());
            servicioCuentas = new ServicioPlanDeCuentas(emf.createEntityManager());
        }
        
        public void crearBalanzaDeComprobacion(){
            service.actualizarBalanza(servicioCuentas.getCuentas());
        }
        
        public PlanDeCuentas getCuenta(String parametro){
            return servicioCuentas.consultar(parametro);
        }
        
        public List<PlanDeCuentas> getCuentas(String parametro){
            return servicioCuentas.consultarList(parametro);
        }
}
