/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.negocio;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javafx.collections.ObservableList;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Tuple;
import sv.edu.ues.fia.persistencia.DetalleTransaccion;
import sv.edu.ues.fia.persistencia.Transaccion;
import sv.edu.ues.fia.servicios.ServicioTransaccion;
/**
 *
 * @author Kevin
 */
public class TransaccionService {
    private final ServicioTransaccion service;
    private final  EntityManagerFactory emf;
    private final AccountService ac;
    private final DateService d;

    public TransaccionService() {
        emf = Persistence.createEntityManagerFactory("base");
        service = new ServicioTransaccion(emf.createEntityManager());
        d = new DateService();
        ac = new AccountService(); 
    }
    
    public boolean agregarTransaccion(Transaction t, ObservableList<TransactionDetail> list){
        boolean f = true;
        List<DetalleTransaccion> lista = new ArrayList<>();
        for(TransactionDetail td : list){
            DetalleTransaccion  aux = new DetalleTransaccion();
            aux.setCodigo(td.getCodigo());

            aux.setDebe(td.getDebe());
            aux.setHaber(td.getHaber());
            aux.setNombre(td.getNombre());
            
            lista.add(aux);
            ac.cargar(td.getCodigo(),td.getDebe());
            ac.abonar(td.getCodigo(),td.getHaber());
            System.out.println("Lista cuentas: " + lista);
        }
        
        Date fecha = Date.from(t.getFecha().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());        
        Transaccion tr = service.crearTransaccion(t.getId(),t.getCommentario(),fecha, lista);
        System.out.println(tr.toString());
        
        if(tr == null){
            f = false;
        }
        return f;
    }
    
    public List<Transaccion> getTransacciones(LocalDate inicio, LocalDate fin){
        Date i = Date.from(inicio.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()); 
        Date f = Date.from(fin.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()); 
        return service.transaccionDiario(i, f);
    }
    
    public List<Mayor> getCuentasMayor(int codigo){
        Date i = Date.from(d.getInicio().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
        Date f = Date.from(d.getFin().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
        return service.getCuentasMayor(codigo, i, f);
    }
}
