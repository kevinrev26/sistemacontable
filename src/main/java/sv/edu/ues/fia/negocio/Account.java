/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.negocio;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Kevin
 */
public class Account {
    private SimpleIntegerProperty codigo;
    private SimpleStringProperty nombre;
    private SimpleBooleanProperty naturaleza;
    private SimpleDoubleProperty saldo;
    
    public Account(){ }
    
    public Account(int codigo, String nombre, boolean naturaleza, double saldo){
        this.codigo = new SimpleIntegerProperty(codigo);
        this.nombre = new SimpleStringProperty(nombre);
        this.naturaleza = new SimpleBooleanProperty(naturaleza);        
        this.saldo = new SimpleDoubleProperty(saldo);
    }
    
    public int getCodigo(){
        return codigo.get();
    }
    
    public void setCodigo(int codigo){
        this.codigo.set(codigo);
    }
    
    public String getNombre(){
        return nombre.get();
    }
    
    public void setNombre(String nombre){
        this.nombre.set(nombre);
    }
    
    public boolean getNaturealeza(){
        return naturaleza.get();
    }
    
    public void setNaturaleza(boolean naturaleza){
        this.naturaleza.set(naturaleza);
    }

    public void setSaldo(double saldo){
        this.saldo.set(saldo);
    }
    
    public double getSaldo(){
        return saldo.getValue();
    }
    
    @Override
    public String toString() {
        return "Account{" + "codigo=" + codigo.get() + ", nombre=" + nombre.get() + ", naturaleza=" + naturaleza.get() + ", saldo=" + saldo.get() + '}';
    }
    
    
}
