/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.negocio;

/**
 *
 * @author Kevin
 */
public class Frijoles {
    private double cantidad;
    private final double precio;

    public Frijoles(double cantidad, double precio) {
        this.cantidad = cantidad;
        this.precio = precio;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    @Override
    public String toString() {
        return "Frijoles{" + "cantidad (En libras)= " + cantidad + ", precio= " + precio + '}';
    }
    
    

    
}
