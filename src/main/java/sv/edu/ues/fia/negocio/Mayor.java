/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.negocio;

import java.util.Date;

/**
 *
 * @author Kevin
 */
public class Mayor {
   // private String nombre;
    //private int codigo;
    private Date fecha;
    private String referencia;
    private double debe;
    private double haber;

    public Mayor(Date fecha, String referencia, double debe, double haber) {
       // this.nombre = nombre;
       // this.codigo = codigo;
        this.fecha = fecha;
        this.referencia = referencia;
        this.debe = debe;
        this.haber = haber;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public double getDebe() {
        return debe;
    }

    public void setDebe(double debe) {
        this.debe = debe;
    }

    public double getHaber() {
        return haber;
    }

    public void setHaber(double haber) {
        this.haber = haber;
    }

    @Override
    public String toString() {
        return "Mayor{" + "fecha=" + fecha + ", referencia=" + referencia + ", debe=" + debe + ", haber=" + haber + '}';
    }

    
    
    
    
}
