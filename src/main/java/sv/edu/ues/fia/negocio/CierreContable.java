/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.negocio;

import java.util.List;
import javafx.scene.control.Alert;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import sv.edu.ues.fia.persistencia.PlanDeCuentas;
import sv.edu.ues.fia.servicios.ServicioPlanDeCuentas;

/**
 *
 * @author Kevin
 */
public class CierreContable {
    
    private final ServicioPlanDeCuentas servicioCuentas;
    double totalDebe = 0.0;
    double totalHaber = 0.0;
    public CierreContable() {
        servicioCuentas = new ServicioPlanDeCuentas(entity());
    }
    
    
    private EntityManager entity(){
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("base");
        return emf.createEntityManager();
    }
    
    public void cerrarPeriodoContable(){
        List<PlanDeCuentas> cuentasIngresos = servicioCuentas.consultarList("4%");
        List<PlanDeCuentas> cuentasGastos = servicioCuentas.consultarList("5%");
        PlanDeCuentas perdidasGanancias = servicioCuentas.buscarCuenta(61101);        
        cerrarCuentas(cuentasIngresos);
        cerrarCuentas(cuentasGastos);
        servicioCuentas.cargar(perdidasGanancias.getCodigo(), totalDebe);
        servicioCuentas.abonar(perdidasGanancias.getCodigo(), totalHaber);
        servicioCuentas.actualizarSaldo(perdidasGanancias.getCodigo(),0.0);
        alerta("Se ha cerrado el periodo contable satisfactoriamente");
       }
    
    private void cerrarCuentas(List<PlanDeCuentas> l){
        for(PlanDeCuentas c : l){
            if(c.getNaturaleza()){
                totalHaber = totalHaber + c.getSaldo();
                servicioCuentas.cargar(c.getCodigo(),c.getSaldo());
            }else{
                totalDebe = totalDebe + c.getSaldo();
                servicioCuentas.abonar(c.getCodigo(),c.getSaldo());
            }
        }
    }
    
    private void alerta(String content){        
                Alert alerta = new Alert(Alert.AlertType.INFORMATION);
                alerta.setHeaderText("Cierre del periodo contable");
                alerta.setContentText(content);
            alerta.showAndWait();            
    }
    

}
