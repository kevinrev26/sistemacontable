/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.negocio;


import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Kevin
 */
public class User {
    private final SimpleStringProperty nombreDeUsuario;
    private final SimpleStringProperty password;
    private final SimpleStringProperty nombreCompleto;

    public String getNombreDeUsuario() {
        return nombreDeUsuario.get();
    }

    public String getPassword() {
        return password.get();
    }

    public String getNombreCompleto() {
        return nombreCompleto.get();
    }
    
    
    public User(String nombreUsuario, String contraseña, String nombreCompleto){
        this.nombreDeUsuario = new SimpleStringProperty(nombreUsuario);
        this.password= new SimpleStringProperty(contraseña);
        this.nombreCompleto = new SimpleStringProperty(nombreCompleto);
    }
    
    
    
}
