/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.negocio;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import sv.edu.ues.fia.persistencia.KardexFrijoles;
import sv.edu.ues.fia.servicios.ServicioKardexFrijoles;

/**
 *
 * @author Kevin
 */
public class KardexCollections {
    private final EntityManagerFactory emf;
    private final ServicioKardexFrijoles servicio;
    private DateService servicioFecha;
    
    public KardexCollections(){
        emf = Persistence.createEntityManagerFactory("base");
        servicio = new ServicioKardexFrijoles(emf.createEntityManager());
        servicioFecha = new DateService();
    }
    
    public ObservableList<Kardex> getKardexFrijoles(){
        Date i = Date.from(servicioFecha.getInicio().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
        Date f = Date.from(servicioFecha.getFin().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
        List<KardexFrijoles> l = servicio.getRegistrosEnElPeriodo(i, f);
        ArrayList<Kardex> lista = new ArrayList<>();
        for(KardexFrijoles k : l){
            Kardex inventario = new Kardex(LocalDateTime.ofInstant(k.getFecha().toInstant(), ZoneId.systemDefault()).toLocalDate(),
                                           k.getCantidadEntrada(),
                                           k.getCostoUnitarioEntrada(),k.getMontoTotalEntrada(),k.getCantidadSalida(),
                                            k.getCostoUnitarioSalida(),k.getMontoTotalSalida(),k.getCantidadExistencia(),
                                            k.getMontoTotalExistencia());
            lista.add(inventario);            
        }
        
        return FXCollections.observableArrayList(lista);
    }
}
