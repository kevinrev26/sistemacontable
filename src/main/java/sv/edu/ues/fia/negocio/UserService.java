/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.negocio;

import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import sv.edu.ues.fia.persistencia.Usuario;
import sv.edu.ues.fia.servicios.ServicioUsuario;

/**
 *
 * @author Kevin
 */
public class UserService {
    private final ServicioUsuario servicio;
    private final EntityManagerFactory emf;
    
    public UserService(){
        emf = Persistence.createEntityManagerFactory("base");
        servicio = new ServicioUsuario(emf.createEntityManager());        
    }
    
    public ObservableList<User> getAllUsers(){
        List<Usuario> listaU = servicio.getTodosUsuarios();
        ArrayList<User> list = new ArrayList<>();
        for (Usuario u : listaU){
            //System.out.println(u.toString()); //debuggin
            User user = new User(u.getUsername(),u.getPassword(),u.getNombre());
            list.add(user);          
        }
       return FXCollections.observableList(list); 
    }
    
}
