/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.negocio;

import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


/**
 *
 * @author Kevin
 */
public class MayorService {
    private final TransaccionService servicio;
 
    
    public MayorService(){
        servicio = new TransaccionService();
        
    }
    
    public ObservableList<Mayor> pruebaT(int codigo){
        
        List<Mayor> list = null;
        ObservableList<Mayor> listaTabla = null;

            list = servicio.getCuentasMayor(codigo);            
            if(list!=null){
                listaTabla =  FXCollections.observableList(list);
            }
        
        return listaTabla;
        }
    
    
    }
    
