/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.negocio;

/**
 *
 * @author Kevin
 */
public class Balanza {
    private int codigo;
    private String nombre;
    private double debe;
    private double haber;

    public Balanza(int codigo, String nombre, double debe, double haber) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.debe = debe;
        this.haber = haber;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getDebe() {
        return debe;
    }

    public void setDebe(double debe) {
        this.debe = debe;
    }

    public double getHaber() {
        return haber;
    }

    public void setHaber(double haber) {
        this.haber = haber;
    }
    
    
}
