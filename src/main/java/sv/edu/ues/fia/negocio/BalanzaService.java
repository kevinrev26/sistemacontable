/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.negocio;

import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import sv.edu.ues.fia.persistencia.BalanzaComprobacion;
import sv.edu.ues.fia.servicios.ServicioBalanza;

/**
 *
 * @author Kevin
 */
public class BalanzaService {
    private final ServicioBalanza service;
    private final EntityManagerFactory emf;
    
    public BalanzaService(){
        emf = Persistence.createEntityManagerFactory("base");
        service = new ServicioBalanza(emf.createEntityManager());
    }
    
    public ObservableList<Balanza> getBalanza(){
        List<BalanzaComprobacion> list = service.recuperarRegistros();
        ArrayList<Balanza> list2=new ArrayList<>();
        
        for(BalanzaComprobacion b : list){
            Balanza balanza = new Balanza(b.getCodigo(),b.getNombre(),b.getDebe(),b.getHaber());
            list2.add(balanza);
        }
        return FXCollections.observableList(list2);
    }
    
    public double getDebe(){ return service.sumaDebe(); }
    
    public double getHaber(){ return service.sumaHaber(); }
    
    
}
