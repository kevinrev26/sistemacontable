/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.negocio;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import sv.edu.ues.fia.persistencia.PeriodoContable;
import sv.edu.ues.fia.servicios.ServicioFecha;

/**
 *
 * @author Kevin
 */
public class DateService {
    private final ServicioFecha servicio;
    private final EntityManagerFactory emf;
    
    public DateService(){
        emf = Persistence.createEntityManagerFactory("base");
        servicio = new ServicioFecha(emf.createEntityManager());
    }
    
    public LocalDate getInicio(){
        List<PeriodoContable> p = servicio.getPeriodo();
        Date d = p.get(0).getInicio();
        return LocalDateTime.ofInstant(d.toInstant(), ZoneId.systemDefault()).toLocalDate();
    }
    public LocalDate getFin(){
        List<PeriodoContable> p = servicio.getPeriodo();
        Date d = p.get(0).getFin();
        return LocalDateTime.ofInstant(d.toInstant(), ZoneId.systemDefault()).toLocalDate();
    
    }
    
    public void actualizarPeriodo(LocalDate inicio,LocalDate fin){
        Date i = Date.from(inicio.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
        Date f = Date.from(fin.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
        PeriodoContable p = new PeriodoContable();
        p.setInicio(i);
        p.setFin(f);
        System.out.println(p.getInicio()+" "+p.getFin());
        servicio.actualizar(p);
    }
}
