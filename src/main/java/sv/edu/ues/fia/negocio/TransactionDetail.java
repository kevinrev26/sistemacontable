/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.negocio;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Kevin
 */
public class TransactionDetail {
    private int codigo;
    private String nombre;
    private double debe;
    private double haber;

    public TransactionDetail(int codigo, String nombre, double debe, double haber) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.debe = debe;
        this.haber = haber;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getDebe() {
        return debe;
    }

    public void setDebe(double debe) {
        this.debe = debe;
    }

    public double getHaber() {
        return haber;
    }

    public void setHaber(double haber) {
        this.haber = haber;
    }

    @Override
    public String toString() {
        return "TransactionDetail{" + "codigo=" + codigo + ", nombre=" + nombre + ", debe=" + debe + ", haber=" + haber + '}';
    }
 
    
}