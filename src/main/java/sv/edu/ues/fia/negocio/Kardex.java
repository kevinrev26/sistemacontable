/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.negocio;

import java.time.LocalDate;

/**
 *
 * @author Kevin
 */
public class Kardex {
    private LocalDate fecha;
    private double cantidadEntrada;
    private double precioUnitarioEntrada;
    private double montoEntrada;
    private double cantidadSalida;
    private double precioUnitarioSalida;
    private double montoSalida;
    private double cantidadExistencia;
    private double montoExistencia;
    
    public Kardex(LocalDate fecha, double cantidadEntrada, double precioUnitarioEntrada, double montoEntrada, double cantidadSalida, double precioUnitarioSalida, double montoSalida, double cantidadExistencia, double montoExistencia) {
        this.fecha = fecha;
        this.cantidadEntrada = cantidadEntrada;
        this.precioUnitarioEntrada = precioUnitarioEntrada;
        this.montoEntrada = montoEntrada;
        this.cantidadSalida = cantidadSalida;
        this.precioUnitarioSalida = precioUnitarioSalida;
        this.montoSalida = montoSalida;
        this.cantidadExistencia = cantidadExistencia;
        this.montoExistencia = montoExistencia;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public double getCantidadEntrada() {
        return cantidadEntrada;
    }

    public void setCantidadEntrada(double cantidadEntrada) {
        this.cantidadEntrada = cantidadEntrada;
    }

    public double getPrecioUnitarioEntrada() {
        return precioUnitarioEntrada;
    }

    public void setPrecioUnitarioEntrada(double precioUnitarioEntrada) {
        this.precioUnitarioEntrada = precioUnitarioEntrada;
    }

    public double getMontoEntrada() {
        return montoEntrada;
    }

    public void setMontoEntrada(double montoEntrada) {
        this.montoEntrada = montoEntrada;
    }

    public double getCantidadSalida() {
        return cantidadSalida;
    }

    public void setCantidadSalida(double cantidadSalida) {
        this.cantidadSalida = cantidadSalida;
    }

    public double getPrecioUnitarioSalida() {
        return precioUnitarioSalida;
    }

    public void setPrecioUnitarioSalida(double precioUnitarioSalida) {
        this.precioUnitarioSalida = precioUnitarioSalida;
    }

    public double getMontoSalida() {
        return montoSalida;
    }

    public void setMontoSalida(double montoSalida) {
        this.montoSalida = montoSalida;
    }

    public double getCantidadExistencia() {
        return cantidadExistencia;
    }

    public void setCantidadExistencia(double cantidadExistencia) {
        this.cantidadExistencia = cantidadExistencia;
    }

    public double getMontoExistencia() {
        return montoExistencia;
    }

    public void setMontoExistencia(double montoExistencia) {
        this.montoExistencia = montoExistencia;
    }
    
}
