/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.negocio;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import sv.edu.ues.fia.persistencia.ColaPrecios;
import sv.edu.ues.fia.persistencia.KardexFrijoles;
import sv.edu.ues.fia.servicios.ServicioKardexFrijoles;

/**
 *
 * @author Kevin
 */
public class OrdenCompra {
    private ServicioKardexFrijoles servicio;
    private EntityManagerFactory emf;
    
    
    public OrdenCompra(){
        emf = Persistence.createEntityManagerFactory("base");
        servicio = new ServicioKardexFrijoles(emf.createEntityManager());
    }
    
    public void registroDeCompra(LocalDate f, double cantidad, double precioU){
           Date i = Date.from(f.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
           servicio.registrarCompra(i, cantidad, precioU);
           servicio.actualizarPrecios();
    }
    
    public void MostrarExistencias(){
        List<KardexFrijoles> lista = servicio.getRegistros();
        for(KardexFrijoles k : lista){
            Frijoles frijoles = new Frijoles(k.getCantidadEntrada(),k.getCostoUnitarioEntrada());
            System.out.println(frijoles);
        }
    }
    
    public void registroSalida(LocalDate f, double cantidad, double precioU){
       Date i = Date.from(f.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
       servicio.registrarSalida(i, cantidad, precioU);
    }
    
    public void actualizarPreciosSalida(int codigo, double cantidad){
        servicio.actualizarPostSalida(codigo, cantidad);
    }
    
    public double getCantidadExistencias(){
        double entrada = servicio.getCantidadEntrada();
        double salida = servicio.getCantidadSalida();
        return entrada-salida;
    }
    
    public List<ColaPrecios> getPrecios(){
        return servicio.getPrecios();
    }
}
