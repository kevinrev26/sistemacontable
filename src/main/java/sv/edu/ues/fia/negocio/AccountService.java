/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.negocio;

import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import sv.edu.ues.fia.persistencia.PlanDeCuentas;
import sv.edu.ues.fia.servicios.ServicioPlanDeCuentas;

/**
 *
 * @author Kevin
 */
public class AccountService {
    private final ServicioPlanDeCuentas servicio;
    private final EntityManagerFactory emf;
    
    public AccountService(){
        emf = Persistence.createEntityManagerFactory("base");
        servicio = new ServicioPlanDeCuentas(emf.createEntityManager()); 
    }
    
    public ObservableList<Account> getAllAccounts(){
        List<PlanDeCuentas> listaU = servicio.getCuentas();
        ArrayList<Account> list = new ArrayList<>();
        for (PlanDeCuentas c : listaU){
            
            Account account = new Account(c.getCodigo(),c.getNombre(),c.getNaturaleza(),c.getSaldo());
            
            list.add(account);          
        }
       return FXCollections.observableList(list); 
    }
        
    public void eliminarCuenta(int codigo){
        servicio.eliminarCuenta(codigo);            
    }
        
    public void actualizarCuenta(Account account){
        PlanDeCuentas cuenta = new PlanDeCuentas(account.getCodigo(),account.getNombre(),account.getNaturealeza(),
                                                 account.getSaldo());
        
        //System.out.println(cuenta.toString());
        servicio.actualizar(cuenta);
    }
    
    public boolean agregarCuenta(Account a){
        boolean bandera = true;
        PlanDeCuentas c = servicio.crearCuenta(a.getCodigo(),a.getNombre(),a.getNaturealeza(),
                                            a.getSaldo());
        if(c==null){
            bandera = false;
        } 
        return bandera;
    }
    
    public ObservableList<String> getNombres(){
        
        List<PlanDeCuentas> listaU = servicio.getCuentas();
        ArrayList<String> list = new ArrayList<>();
       for (PlanDeCuentas c : listaU){
            String nombre = "";
            nombre = c.getNombre();
            //System.out.println("Cuenta numero: "+c.getCodigo()); //debugging
            
            list.add(nombre);          
        }
       return FXCollections.observableList(list);
    }
    
    public Account  buscarPorNombre(String nombre){
        PlanDeCuentas cuenta = servicio.buscarPorNombre(nombre);
        
        return new Account(cuenta.getCodigo(),cuenta.getNombre(),cuenta.getNaturaleza(),cuenta.getSaldo());
    }
    
    public void abonar(int codigo,Double cantidad){
        servicio.abonar(codigo, cantidad);
    }
    
    public void cargar(int codigo,Double cantidad){
        servicio.cargar(codigo, cantidad);
    }
}
