/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.sistemacontable;



import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import sv.edu.ues.fia.controladores.LoginController;

/**
 *
 * @author Kevin
 */
public class MainApp extends Application{
    public static void main(String[] args) {
        launch(args);
    }
    
    @Override
    public void start(Stage stage) throws IOException{
    //setUserAgentStylesheet(STYLESHEET_MODENA);
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(MainApp.class.getResource("/fxml/login.fxml"));
        Parent raiz = loader.load();
        LoginController controlador = loader.getController();
        controlador.setStage(stage);
        Scene scene  = new Scene(raiz);
        stage.setScene(scene);        
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setResizable(false);
        stage.show();
  

    
    }
}
