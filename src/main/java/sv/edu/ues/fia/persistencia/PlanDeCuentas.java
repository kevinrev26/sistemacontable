/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.persistencia;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Kevin
 */
@Entity
@Table(name = "plan_de_cuentas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PlanDeCuentas.findByCodigo", query = "SELECT p FROM PlanDeCuentas p WHERE p.codigo = :codigo"),
    @NamedQuery(name = "PlanDeCuentas.findByNombre", query = "SELECT p FROM PlanDeCuentas p WHERE p.nombre = :nombre"),
    @NamedQuery(name = "PlanDeCuentas.findByNaturaleza", query = "SELECT p FROM PlanDeCuentas p WHERE p.naturaleza = :naturaleza"),
    @NamedQuery(name = "PlanDeCuentas.findBySaldo", query = "SELECT p FROM PlanDeCuentas p WHERE p.saldo = :saldo"),//})
    @NamedQuery(name = "PlanDeCuentas.consultar", query="SELECT p FROM PlanDeCuentas p WHERE CAST(p.codigo AS TEXT) LIKE :param ORDER BY p.codigo ASC")})
public class PlanDeCuentas implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "codigo")
    private Integer codigo;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "naturaleza")
    private boolean naturaleza;
    @Basic(optional = false)
    @Column(name = "saldo")
    private double saldo;

    public PlanDeCuentas() {
    }

    public PlanDeCuentas(Integer codigo) {
        this.codigo = codigo;
    }

    public PlanDeCuentas(Integer codigo, String nombre, boolean naturaleza, double saldo) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.naturaleza = naturaleza;
        this.saldo = saldo;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean getNaturaleza() {
        return naturaleza;
    }

    public void setNaturaleza(boolean naturaleza) {
        this.naturaleza = naturaleza;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigo != null ? codigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlanDeCuentas)) {
            return false;
        }
        PlanDeCuentas other = (PlanDeCuentas) object;
        if ((this.codigo == null && other.codigo != null) || (this.codigo != null && !this.codigo.equals(other.codigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.edu.ues.fia.persistencia.PlanDeCuentas[ codigo=" + codigo + " ]";
    }
    
}
