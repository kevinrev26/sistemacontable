/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.persistencia;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Kevin
 */
@Entity
@Table(name = "cola_precios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ColaPrecios.findAll", query = "SELECT c FROM ColaPrecios c ORDER BY c.id ASC"),
    @NamedQuery(name = "ColaPrecios.findById", query = "SELECT c FROM ColaPrecios c WHERE c.id = :id"),
    @NamedQuery(name = "ColaPrecios.findByCantidad", query = "SELECT c FROM ColaPrecios c WHERE c.cantidad = :cantidad"),
    @NamedQuery(name = "ColaPrecios.findByPrecioUnitario", query = "SELECT c FROM ColaPrecios c WHERE c.precioUnitario = :precioUnitario")})
public class ColaPrecios implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "cantidad")
    private double cantidad;
    @Basic(optional = false)
    @Column(name = "precio_unitario")
    private double precioUnitario;

    public ColaPrecios() {
    }

    public ColaPrecios(Integer id) {
        this.id = id;
    }

    public ColaPrecios(Integer id, double cantidad, double precioUnitario) {
        this.id = id;
        this.cantidad = cantidad;
        this.precioUnitario = precioUnitario;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(double precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ColaPrecios)) {
            return false;
        }
        ColaPrecios other = (ColaPrecios) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    public double getCantidadEsp(double  q){
        
        setCantidad(getCantidad()-q);
        return q;
    }
    
    @Override
    public String toString() {
        return "ColaPrecios{" + "id=" + id + ", cantidad=" + cantidad + ", precioUnitario=" + precioUnitario + '}';
    }


    
}
