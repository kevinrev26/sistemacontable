/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.persistencia;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Kevin
 */
@Entity
@Table(name = "kardex_frijoles")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "KardexFrijoles.findAll", query = "SELECT k FROM KardexFrijoles k"),
    @NamedQuery(name = "KardexFrijoles.findByCantidadEntrada", query = "SELECT k FROM KardexFrijoles k WHERE k.cantidadEntrada = :cantidadEntrada"),
    @NamedQuery(name = "KardexFrijoles.findByCostoUnitarioEntrada", query = "SELECT k FROM KardexFrijoles k WHERE k.costoUnitarioEntrada = :costoUnitarioEntrada"),
    @NamedQuery(name = "KardexFrijoles.findByMontoTotalEntrada", query = "SELECT k FROM KardexFrijoles k WHERE k.montoTotalEntrada = :montoTotalEntrada"),
    @NamedQuery(name = "KardexFrijoles.findByCantidadSalida", query = "SELECT k FROM KardexFrijoles k WHERE k.cantidadSalida = :cantidadSalida"),
    @NamedQuery(name = "KardexFrijoles.findById", query = "SELECT k FROM KardexFrijoles k WHERE k.id = :id"),
    @NamedQuery(name = "KardexFrijoles.findByFechas", query = "SELECT k FROM KardexFrijoles k WHERE k.fecha BETWEEN :inicio AND :fin ORDER BY k.fecha ASC"),
    @NamedQuery(name = "KardexFrijoles.findByMontoTotalSalida", query = "SELECT k FROM KardexFrijoles k WHERE k.montoTotalSalida = :montoTotalSalida"),
    @NamedQuery(name = "KardexFrijoles.findByCantidadExistencia", query = "SELECT k FROM KardexFrijoles k WHERE k.cantidadExistencia = :cantidadExistencia"),
    @NamedQuery(name = "KardexFrijoles.findByMontoTotalExistencia", query = "SELECT k FROM KardexFrijoles k WHERE k.montoTotalExistencia = :montoTotalExistencia"),
    @NamedQuery(name = "KardexFrijoles.findByFecha", query = "SELECT k FROM KardexFrijoles k WHERE k.fecha = :fecha")})
public class KardexFrijoles implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "cantidad_entrada")
    private double cantidadEntrada;
    @Basic(optional = false)
    @Column(name = "costo_unitario_entrada")
    private double costoUnitarioEntrada;
    @Basic(optional = false)
    @Column(name = "monto_total_entrada")
    private double montoTotalEntrada;
    @Basic(optional = false)
    @Column(name = "cantidad_salida")
    private double cantidadSalida;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "costo_unitario_salida")
    private double costoUnitarioSalida;
    @Basic(optional = false)
    @Column(name = "monto_total_salida")
    private double montoTotalSalida;
    @Basic(optional = false)
    @Column(name = "cantidad_existencia")
    private double cantidadExistencia;
    @Basic(optional = false)
    @Column(name = "monto_total_existencia")
    private double montoTotalExistencia;
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;

    public KardexFrijoles() {
    }

    public KardexFrijoles(Integer id) {
        this.id = id;
    }

    public KardexFrijoles(Integer id, double cantidadEntrada, double costoUnitarioEntrada, double montoTotalEntrada, double cantidadSalida, double costoUnitarioSalida, double montoTotalSalida, double cantidadExistencia, double montoTotalExistencia) {
        this.id = id;
        this.cantidadEntrada = cantidadEntrada;
        this.costoUnitarioEntrada = costoUnitarioEntrada;
        this.montoTotalEntrada = montoTotalEntrada;
        this.cantidadSalida = cantidadSalida;
        this.costoUnitarioSalida = costoUnitarioSalida;
        this.montoTotalSalida = montoTotalSalida;
        this.cantidadExistencia = cantidadExistencia;
        this.montoTotalExistencia = montoTotalExistencia;
    }

    public double getCantidadEntrada() {
        return cantidadEntrada;
    }

    public void setCantidadEntrada(double cantidadEntrada) {
        this.cantidadEntrada = cantidadEntrada;
    }

    public double getCostoUnitarioEntrada() {
        return costoUnitarioEntrada;
    }

    public void setCostoUnitarioEntrada(double costoUnitarioEntrada) {
        this.costoUnitarioEntrada = costoUnitarioEntrada;
    }

    public double getMontoTotalEntrada() {
        return montoTotalEntrada;
    }

    public void setMontoTotalEntrada(double montoTotalEntrada) {
        this.montoTotalEntrada = montoTotalEntrada;
    }

    public double getCantidadSalida() {
        return cantidadSalida;
    }

    public void setCantidadSalida(double cantidadSalida) {
        this.cantidadSalida = cantidadSalida;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getCostoUnitarioSalida() {
        return costoUnitarioSalida;
    }

    public void setCostoUnitarioSalida(double costoUnitarioSalida) {
        this.costoUnitarioSalida = costoUnitarioSalida;
    }

    public double getMontoTotalSalida() {
        return montoTotalSalida;
    }

    public void setMontoTotalSalida(double montoTotalSalida) {
        this.montoTotalSalida = montoTotalSalida;
    }

    public double getCantidadExistencia() {
        return cantidadExistencia;
    }

    public void setCantidadExistencia(double cantidadExistencia) {
        this.cantidadExistencia = cantidadExistencia;
    }

    public double getMontoTotalExistencia() {
        return montoTotalExistencia;
    }

    public void setMontoTotalExistencia(double montoTotalExistencia) {
        this.montoTotalExistencia = montoTotalExistencia;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof KardexFrijoles)) {
            return false;
        }
        KardexFrijoles other = (KardexFrijoles) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.edu.ues.fia.persistencia.KardexFrijoles[ id=" + id + " ]";
    }
    
}
