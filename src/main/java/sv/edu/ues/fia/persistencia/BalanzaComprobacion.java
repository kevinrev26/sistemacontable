/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.persistencia;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Kevin
 */
@Entity
@Table(name = "balanza_comprobacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BalanzaComprobacion.findAll", query = "SELECT b FROM BalanzaComprobacion b ORDER BY b.codigo ASC"),
    @NamedQuery(name = "BalanzaComprobacion.findById", query = "SELECT b FROM BalanzaComprobacion b WHERE b.id = :id"),
    @NamedQuery(name = "BalanzaComprobacion.findByCodigo", query = "SELECT b FROM BalanzaComprobacion b WHERE b.codigo = :codigo"),
    @NamedQuery(name = "BalanzaComprobacion.findByNombre", query = "SELECT b FROM BalanzaComprobacion b WHERE b.nombre = :nombre"),
    @NamedQuery(name = "BalanzaComprobacion.findByDebe", query = "SELECT b FROM BalanzaComprobacion b WHERE b.debe = :debe"),
    @NamedQuery(name = "BalanzaComprobacion.findByHaber", query = "SELECT b FROM BalanzaComprobacion b WHERE b.haber = :haber")})
public class BalanzaComprobacion implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "codigo")
    private int codigo;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "debe")
    private Double debe;
    @Column(name = "haber")
    private Double haber;

    public BalanzaComprobacion() {
    }

    public BalanzaComprobacion(Integer id) {
        this.id = id;
    }

    public BalanzaComprobacion(Integer id, int codigo, String nombre) {
        this.id = id;
        this.codigo = codigo;
        this.nombre = nombre;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getDebe() {
        return debe;
    }

    public void setDebe(Double debe) {
        this.debe = debe;
    }

    public Double getHaber() {
        return haber;
    }

    public void setHaber(Double haber) {
        this.haber = haber;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BalanzaComprobacion)) {
            return false;
        }
        BalanzaComprobacion other = (BalanzaComprobacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.edu.ues.fia.persistencia.BalanzaComprobacion[ id=" + id + " ]";
    }
    
}
