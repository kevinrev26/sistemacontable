/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.controladores;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javax.persistence.EntityManagerFactory;
import sv.edu.ues.fia.negocio.User;
import sv.edu.ues.fia.negocio.UserService;
import sv.edu.ues.fia.persistencia.Usuario;
import sv.edu.ues.fia.servicios.ServicioUsuario;

/**
 * FXML Controller class
 *
 * @author Kevin
 */
public class UsuariosController implements Initializable {
    @FXML
    private TableView<User> tablaUsuarios;
    @FXML
    private TableColumn<User,String> colNombreUsuario;
    @FXML
    private TableColumn<User, String> colPass;
    @FXML
    private TableColumn<User, String> colNombre;
    @FXML
    private Button agregar;
    @FXML
    private Button eliminar;
    @FXML
    private Button editar;
    private UserService us;


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        this.eliminar.setDisable(true);
        this.editar.setDisable(true);        
        this.us = new UserService();
        
        this.colNombreUsuario.setCellValueFactory(new PropertyValueFactory<>("nombreDeUsuario"));
        this.colPass.setCellValueFactory(new PropertyValueFactory<>("password"));
        this.colNombre.setCellValueFactory(new PropertyValueFactory<>("nombreCompleto"));
        this.tablaUsuarios.setItems(us.getAllUsers());
        
    }
    

    
}
