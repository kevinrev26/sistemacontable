/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.controladores;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import sv.edu.ues.fia.negocio.Account;
import sv.edu.ues.fia.negocio.AccountService;

/**
 * FXML Controller class
 *
 * @author Kevin
 */
public class PlanDeCuentasController implements Initializable {
    @FXML
    private HBox raiz;
    @FXML
    private TableView<Account> tablaCuentas;
    @FXML
    private TableColumn<Account, Integer> codigoColumn;
    @FXML
    private TableColumn<Account, String> nombreColumn;

    @FXML
    private TableColumn<Account,Double> saldoColoumn;
    @FXML
    private TextField codigoText;
    @FXML
    private TextField nombreText;
    @FXML
    private ComboBox<String> naturalezaCombo;
    @FXML
    private TextField saldoText;
    @FXML
    private Button nuevo;
    @FXML
    private Button actualizar;
    @FXML
    private Button eliminar;
    @FXML
    private VBox panelEdicion;
    private AccountService as;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        this.naturalezaCombo.getItems().addAll("Deudora","Acreedora");
        
        this.as = new AccountService();
            actualizar.setDisable(true);
            eliminar.setDisable(true);
            this.codigoColumn.setCellValueFactory(new PropertyValueFactory<>("codigo"));
            this.nombreColumn.setCellValueFactory(new PropertyValueFactory<>("nombre"));            
            this.saldoColoumn.setCellValueFactory(new PropertyValueFactory<>("saldo"));
            actualizarTabla();
    }    

    @FXML
    private void seleccionarFila(MouseEvent event) {
        Account seleccionado = tablaCuentas.getSelectionModel().getSelectedItem();
        nuevo.setDisable(true);
        actualizar.setDisable(false);
        eliminar.setDisable(false);
        
        nuevo.setDisable(true);
        codigoText.setText(String.valueOf(seleccionado.getCodigo()));
        nombreText.setText(seleccionado.getNombre());
        saldoText.setText(String.valueOf(seleccionado.getSaldo()));
        if(seleccionado.getNaturealeza()){
            naturalezaCombo.getSelectionModel().select("Acreedora");
        } else {
            naturalezaCombo.getSelectionModel().select("Deudora");
        }
    }
    


    @FXML
    private void eliminarCuenta(ActionEvent event) {
        Optional<ButtonType> resultado = confirmacion();
        if(resultado.get() == ButtonType.OK){
            int codigo = Integer.parseInt(codigoText.getText());
            as.eliminarCuenta(codigo);
            alertaConfirmacion();
        }
        actualizarTabla();
    }
    
    private Optional<ButtonType> confirmacion(){
        Alert alerta = new Alert(AlertType.CONFIRMATION);
        alerta.setTitle("Confirmacion");
        alerta.setHeaderText("La siguiente cuenta será eliminada: "+nombreText.getText());
        alerta.setContentText("¿Está seguro de realizar esta acción?");
        return alerta.showAndWait();               
    }
    
    private void alertaConfirmacion(){
        Alert alerta = new Alert(AlertType.INFORMATION);
        alerta.setTitle("Aviso de modificacion en el catalogo de cuentas");
        alerta.setContentText("La cuenta ha sido eliminada con exito");
        alerta.showAndWait();
    }

    @FXML
    private void actualizar(ActionEvent event) {
        
        int codigo = Integer.parseInt(codigoText.getText());
        String nombre = nombreText.getText();
        Double saldo = Double.parseDouble(saldoText.getText());
        boolean naturaleza;
        String comobox = naturalezaCombo.getSelectionModel().getSelectedItem();
        if(comobox.equalsIgnoreCase("acreedora")){
            naturaleza = true;        
        }else{
            naturaleza = false;        
        }        
        Account seleccionado = new Account(codigo,nombre,naturaleza,saldo);               
        
        System.out.println(seleccionado.toString());                           
        as.actualizarCuenta(seleccionado);
        Alert alerta = new Alert(AlertType.INFORMATION);
        alerta.setHeaderText("Seleccionada: "+nombre);
        alerta.setContentText("Se ha actualizado la cuenta");
        alerta.showAndWait();
        actualizarTabla();
    }
    
    private void actualizarTabla(){
        this.tablaCuentas.setItems(as.getAllAccounts());
    }

    @FXML
    private void crearCuenta(ActionEvent event) {
       int codigo=0;
       String nombre="";
       double saldo=0.0;
        if(codigoText.getText().isEmpty()){
            alerta("El campo de codigo se encuentra vacío");
            return;
        } else {
            if(codigoText.getText().matches("[0-9]+") && 
                    codigoText.getText().length()==5){
                codigo = Integer.parseInt(codigoText.getText());
                //System.out.println(codigo);
                          
            } else {
                alerta("El campo codigo contiene letras o no contiene cinco digitos");  
            }//Validacion de letras y verificacion de vacuidad
        
        }//Codigo
        
        if(nombreText.getText().isEmpty()){
            alerta("El campo nombre se encuentra vacio");
            return;
        } else {
            if(nombreText.getText().matches("[0-9]+")){
                alerta("El campo nombre contiene valores numericos");                
            } else {
                nombre = nombreText.getText();
                //System.out.println("Cuenta nombre: "+nombre);
            }
        
        }//Validacion nombre
        
        if(saldoText.getText().isEmpty()){
            alerta("El campo saldo esta vacio");
            return;
        } else {
            if(saldoText.getText().matches("[0-9]+([.][0-9]+)?")){
                saldo = Double.parseDouble(saldoText.getText());
                //System.out.println(saldo);
            } else {
                alerta("El campo saldo contiene letras");
            }
        }//Saldo
        
        boolean naturaleza=false;
        String comobox = naturalezaCombo.getSelectionModel().getSelectedItem();
        if(comobox.isEmpty()){
            alerta("No se ha seleccionado la naturaleza de la cuenta");
        } else {
            if(comobox.equalsIgnoreCase("acreedora")){
                naturaleza = true;        
            }else{
                naturaleza = false;        
            } 
        }//ComboBox
        
        Account c = new Account(codigo,nombre,naturaleza,saldo);
        if(as.agregarCuenta(c)){
            alerta("Cuenta agregada con exito");
        } else {
            error("No se ha podido agregar la cuenta: "+nombre);
        }
        actualizarTabla();
    }
    

    
    private void alerta(String content){        
                Alert alerta = new Alert(AlertType.INFORMATION);
                alerta.setHeaderText("Agregar nueva cuenta al catalogo");
                alerta.setContentText(content);
            alerta.showAndWait();            
    }
    
    private void error(String content){
        Alert alerta = new Alert(AlertType.ERROR);
        alerta.setHeaderText("Ocurrio algo inesperado");
        alerta.setContentText(content);
        alerta.showAndWait();    
    }
}
