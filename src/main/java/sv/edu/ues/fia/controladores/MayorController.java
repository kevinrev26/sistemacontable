/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.controladores;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import sv.edu.ues.fia.negocio.Account;
import sv.edu.ues.fia.negocio.AccountService;
import sv.edu.ues.fia.negocio.DateService;
import sv.edu.ues.fia.negocio.MayorService;

/**
 * FXML Controller class
 *
 * @author Kevin
 */
public class MayorController implements Initializable {
    @FXML
    private Text periodoText;
    private DateService service;
    private MayorService mayorService;
    private AccountService ac;
    @FXML
    private ListView<VBox> listaMayor;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        service = new DateService();
        mayorService = new MayorService();
        ac = new AccountService();
        periodoText.setText(periodo());
        try {
            getCuentas();
        } catch (IOException ex) {
            Logger.getLogger(MayorController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }    
    
    private String periodo(){        
        LocalDate inicio = service.getInicio();
        LocalDate fin = service.getFin();
        return "De "+inicio.getDayOfMonth()+" de "+inicio.getMonth()+" al "+
                fin.getDayOfMonth()+" de "+fin.getMonth()+" del "+fin.getYear();
        
    }
    
    public void  getCuentas() throws IOException{
        
        ObservableList<Account> lista = ac.getAllAccounts();
        for(Account a : lista){            
           if(null!= mayorService.pruebaT(a.getCodigo())){ 
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(MayorController.class.getResource("/fxml/MayorVoucher.fxml"));
                VBox v = loader.load();
                final MayorVoucherController voucher = loader.getController();
                voucher.setCodigo(a.getCodigo());
                voucher.setNombre(a.getNombre());
                voucher.setTableItems(mayorService.pruebaT(a.getCodigo()));
                listaMayor.getItems().add(v);
               
           }
        }
    
    }
}
