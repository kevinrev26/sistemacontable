/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.controladores;

import java.net.URL;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import sv.edu.ues.fia.negocio.Balanza;
import sv.edu.ues.fia.negocio.BalanzaService;
import sv.edu.ues.fia.negocio.DateService;


/**
 * FXML Controller class
 *
 * @author Kevin
 */
public class BalanzaDeComprobacionController implements Initializable {
    @FXML
    private Text periodoText;
    @FXML
    private Text totalDebe;
    @FXML
    private Text totalHaber;
    @FXML
    private TableView<Balanza> balance;
    @FXML
    private TableColumn<Balanza,Integer> columnaCodigo;
    @FXML
    private TableColumn<Balanza,String> columnaCuenta;
    @FXML
    private TableColumn<Balanza,Double> columnaDebe;
    @FXML
    private TableColumn<Balanza, Double> columnaHaber;
    private DateService service;
    private BalanzaService bs;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        service=new DateService();
        bs = new BalanzaService();
        double debe = 0.0;
        double haber = 0.0;
        debe = bs.getDebe();
        haber = bs.getHaber();
        columnaCodigo.setCellValueFactory(new PropertyValueFactory<>("codigo"));
        columnaCuenta.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        columnaDebe.setCellValueFactory(new PropertyValueFactory<>("debe"));
        columnaHaber.setCellValueFactory(new PropertyValueFactory<>("haber"));
        NumberFormat formateo = NumberFormat.getCurrencyInstance(Locale.US);
        totalDebe.setText(formateo.format(debe));
        totalHaber.setText(formateo.format(haber));
        String texto = "";
        LocalDate inicio = service.getInicio();
        LocalDate fin = service.getFin();      
        texto="De "+inicio.getDayOfMonth()+" de "+inicio.getMonth()+" al "+
                fin.getDayOfMonth()+" de "+fin.getMonth()+" del "+fin.getYear();
        periodoText.setText(texto);
        balance.setItems(bs.getBalanza());
        
    }    
    
}
