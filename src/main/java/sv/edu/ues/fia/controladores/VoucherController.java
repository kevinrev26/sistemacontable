/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.controladores;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author Kevin
 */
public class VoucherController implements Initializable {
    
    @FXML
    private VBox contenedor;
    @FXML
    private Text idTransaccionText;
    @FXML
    private Text fechaText;
    @FXML
    private TextArea comentarioText;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    public void setTransaccion(String id){
        idTransaccionText.setText(id);
    }
    
    public void setFecha(String fecha){
       fechaText.setText(fecha);
    }
    
    public void setComentario(String comentario){
        comentarioText.setText(comentario);
    }
    
    public void setBackgroundColor(String style){contenedor.setStyle(style);}
    
    public VBox getRaiz(){return contenedor;}
}
