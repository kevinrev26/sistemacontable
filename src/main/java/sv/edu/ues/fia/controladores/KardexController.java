/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.controladores;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import sv.edu.ues.fia.negocio.DateService;
import sv.edu.ues.fia.negocio.Kardex;

/**
 * FXML Controller class
 *
 * @author Kevin
 */
public class KardexController implements Initializable {
    @FXML
    private Text encabezado;
    @FXML
    private Text periodo;
    @FXML
    private TableView<Kardex> kardex;
    @FXML
    private TableColumn<Kardex, LocalDate> fecha;
    @FXML
    private TableColumn<Kardex, Double> qEntrada;
    @FXML
    private TableColumn<Kardex,Double> precioUnitarioEntrada;
    @FXML
    private TableColumn<Kardex,Double> montoEntrada;
    @FXML
    private TableColumn<Kardex, Double> qSalida;
    @FXML
    private TableColumn<Kardex, Double> precioUnitarioSalida;
    @FXML
    private TableColumn<Kardex,Double> montoSalida;
    @FXML
    private TableColumn<Kardex, Double> qExistencia;
    @FXML
    private TableColumn<Kardex, Double> saldoExistencia;
    private DateService service;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        service=new DateService();
        fecha.setCellValueFactory(new PropertyValueFactory<>("fecha"));
        qEntrada.setCellValueFactory(new PropertyValueFactory<>("cantidadEntrada"));
        precioUnitarioEntrada.setCellValueFactory(new PropertyValueFactory<>("precioUnitarioEntrada"));
        montoEntrada.setCellValueFactory(new PropertyValueFactory<>("montoEntrada"));
        qSalida.setCellValueFactory(new PropertyValueFactory<>("cantidadSalida"));
        precioUnitarioSalida.setCellValueFactory(new PropertyValueFactory<>("precioUnitarioSalida"));
        montoSalida.setCellValueFactory(new PropertyValueFactory<>("montoSalida"));
        qExistencia.setCellValueFactory(new PropertyValueFactory<>("cantidadExistencia"));
        saldoExistencia.setCellValueFactory(new PropertyValueFactory<>("montoExistencia"));
        periodo.setText(periodo());
        
    }
    
        private String periodo(){
        LocalDate inicio = service.getInicio();
        LocalDate fin = service.getFin();      
        return "De "+inicio.getDayOfMonth()+" de "+inicio.getMonth()+" al "+
                fin.getDayOfMonth()+" de "+fin.getMonth()+" del "+fin.getYear();
    }
    
        
    public void SetTable(ObservableList<Kardex> list){
        this.kardex.setItems(list);
    }
    
    public void setEncabezado(String encabezado){ this.encabezado.setText(encabezado);}
}
