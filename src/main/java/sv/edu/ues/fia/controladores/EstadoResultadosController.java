/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.controladores;

import java.net.URL;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.text.Text;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import sv.edu.ues.fia.negocio.Account;
import sv.edu.ues.fia.negocio.AccountService;
import sv.edu.ues.fia.negocio.DateService;
import sv.edu.ues.fia.persistencia.PlanDeCuentas;
import sv.edu.ues.fia.servicios.ServicioPlanDeCuentas;

/**
 * FXML Controller class
 *
 * @author Kevin
 */
public class EstadoResultadosController implements Initializable {
    @FXML
    private Text periodo;
    @FXML
    private Text ingresoVenta;
    @FXML
    private Text costoVendido;
    @FXML
    private Text gastosVentas;
    @FXML
    private Text gastosAdmin;
    @FXML
    private Text gastosGenerales;
    @FXML
    private Text otrosIngresos;
    @FXML
    private Text otrosGastos;
    @FXML
    private Text utilidadOperacional;
    @FXML
    private Text gastosFinancieros;
    @FXML
    private Text uai;
    @FXML
    private Text isr;
    @FXML
    private Text utilidadEjercicio;
    private DateService service;
    @FXML
    private Text utilidadBruta;
    private ServicioPlanDeCuentas servicioCuentas;
    private AccountService ac;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        service=new DateService();
        ac = new AccountService();
        servicioCuentas = new ServicioPlanDeCuentas(entity());
        periodo.setText(periodo());
        PlanDeCuentas ventas = servicioCuentas.consultar("41101");
        PlanDeCuentas costoVen = servicioCuentas.consultar("53101");
        PlanDeCuentas impuesto = servicioCuentas.consultar("56101");        
        ingresoVenta.setText(formateo(ventas.getSaldo()));
        costoVendido.setText(formateo(costoVen.getSaldo()*(-1.0)));
        double ut = ventas.getSaldo() - costoVen.getSaldo();
        utilidadBruta.setText(formateo(ut));
        double totalGastosVentas = total("42%",-1.0);
        gastosVentas.setText(formateo(totalGastosVentas));
        double totalGastosAdmin = total("521%",-1.0);
        gastosAdmin.setText(formateo(totalGastosAdmin));
        double totalGastosGenerales = total("511%",-1.0);
        gastosGenerales.setText(formateo(totalGastosGenerales));
        double totalOtrosGastos = total("551%",-1.0);
        otrosGastos.setText(formateo(totalOtrosGastos));
        double totalOtrosIngresos = total("431%",1.0);
        otrosIngresos.setText(formateo(totalOtrosIngresos));
        double  up = totalGastosVentas + totalGastosGenerales + totalGastosAdmin
                + totalOtrosGastos + totalOtrosIngresos + ut;
        utilidadOperacional.setText(formateo(up));
        double totalGastosFinancieros = total("541%",-1.0);
        gastosFinancieros.setText(formateo(totalGastosFinancieros));
        double uantes = up + totalGastosFinancieros;
        uai.setText(formateo(uantes));
        double imp = Math.abs(uantes)*0.01;
        ac.cargar(impuesto.getCodigo(),imp);
        isr.setText(formateo(imp*-1.0));
        double utilidad = uantes - imp;
        utilidadEjercicio.setText(formateo(utilidad));
        servicioCuentas.grabarUtilidad(utilidad);
        
    }    
    
    private String periodo(){
        LocalDate inicio = service.getInicio();
        LocalDate fin = service.getFin();      
        return "De "+inicio.getDayOfMonth()+" de "+inicio.getMonth()+" al "+
                fin.getDayOfMonth()+" de "+fin.getMonth()+" del "+fin.getYear();
    }
    
    private EntityManager entity(){
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("base");
        return emf.createEntityManager();
    }
    
    private String formateo(double d){
        NumberFormat formateo = NumberFormat.getCurrencyInstance(Locale.US);
        return formateo.format(d);
    }
    
    private double total(String param, double factor){
        List<PlanDeCuentas> lista = servicioCuentas.consultarList(param);
        double suma =  0.0;
        for(PlanDeCuentas c : lista){
            suma = suma + c.getSaldo();
        }
        
        return suma*factor;
    }
    

}
