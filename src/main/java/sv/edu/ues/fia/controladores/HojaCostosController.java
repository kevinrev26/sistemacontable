/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.controladores;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import sv.edu.ues.fia.negocio.AccountService;
import sv.edu.ues.fia.negocio.OrdenCompra;
import sv.edu.ues.fia.persistencia.ColaPrecios;

/**
 * FXML Controller class
 *
 * @author Kevin
 */
public class HojaCostosController implements Initializable {
    @FXML
    private Text cantidadExistencia;
    @FXML
    private TextField cantidadAsignada;
    @FXML
    private TextField modText;
    @FXML
    private TextField cifText;
    private OrdenCompra orden;
    private AccountService ac;
    @FXML
    private DatePicker fecha;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        orden = new OrdenCompra();
        ac = new AccountService();
        cantidadExistencia.setText(String.valueOf(orden.getCantidadExistencias()));
    }    

    @FXML
    private void completarAsignacion(ActionEvent event) {
        double mod = Double.parseDouble(modText.getText());
        ac.abonar(21105, mod);
        ac.cargar(11309, mod);
        double tasaCif = (Double.parseDouble(cifText.getText()))/100;
        ac.abonar(62101, tasaCif*mod);
        ac.cargar(11309, tasaCif*mod);
        salida();
        //mostrar();
    }
    
    public void salida(){
        double qR = Double.parseDouble(cantidadAsignada.getText());
        double auxQ = 0.0;
        double precioU = 0.0;
        List<ColaPrecios> lista = orden.getPrecios();
        for(ColaPrecios c : lista){
            if(c.getCantidad()>0){
                if(c.getCantidad()>=qR){
                    auxQ = c.getCantidadEsp(qR);
                    precioU = c.getPrecioUnitario();
                    orden.registroSalida(fecha.getValue(), auxQ, precioU);
                    //c.setCantidad(auxQ-qR);
                    orden.actualizarPreciosSalida(c.getId(), c.getCantidad());
                    ac.abonar(11301, precioU*auxQ);
                    ac.cargar(11301, precioU*auxQ);
                    return;
                }else{
                    auxQ = c.getCantidad();
                    precioU = c.getPrecioUnitario();
                    qR = qR - c.getCantidad();
                    c.setCantidad(0);
                    orden.actualizarPreciosSalida(c.getId(), c.getCantidad());
                    orden.registroSalida(fecha.getValue(), auxQ, precioU);
                    ac.abonar(11301, precioU*auxQ);
                    ac.cargar(11301, precioU*auxQ);
                }//Segunda comparacion 
            }//If de cantidad
        }
    }
    public void mostrar(){
        double qR = Double.parseDouble(cantidadAsignada.getText());
        int i = 0;
        List<ColaPrecios> lista = orden.getPrecios();
        for (ColaPrecios c : lista){
            if(c.getCantidad()>0){
                i = i+1;
                System.out.println("If mayor que cero "+i);
                if(c.getCantidad()>=qR){
                    System.out.println("If Mayor o igual a qR " +i);
                    //auxQ = c.getCantidad();
                    //precioU = c.getPrecioUnitario();
                    //orden.registroSalida(fecha.getValue(), auxQ, precioU);
                    //c.setCantidad(auxQ-qR);
                    //orden.actualizarPreciosSalida(c.getId(), c.getCantidad());
                    //ac.abonar(11301, precioU*auxQ);
                    //ac.cargar(11301, precioU*auxQ);
                    
                }else{
                    System.out.println("qR es mayor " +i);
                    Double auxQ = c.getCantidad();
                    //precioU = c.getPrecioUnitario();
                    qR = qR - auxQ;
                    //c.setCantidad(0);
                    //orden.actualizarPreciosSalida(c.getId(), c.getCantidad());
                    //orden.registroSalida(fecha.getValue(), auxQ, precioU);
                    //ac.abonar(11301, precioU*auxQ);
                    //ac.cargar(11301, precioU*auxQ);
                }//Segunda comparacion 
            }
        }
    }
}
