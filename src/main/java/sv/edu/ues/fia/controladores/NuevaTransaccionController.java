/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.controladores;

import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import sv.edu.ues.fia.negocio.AccountService;
import sv.edu.ues.fia.negocio.TransactionDetail;
import sv.edu.ues.fia.negocio.Account;
import sv.edu.ues.fia.negocio.DateService;
import sv.edu.ues.fia.negocio.TransaccionService;
import sv.edu.ues.fia.negocio.Transaction;

/**
 * FXML Controller class
 *
 * @author Kevin
 */
public class NuevaTransaccionController implements Initializable {
    @FXML
    private Text idText;
    @FXML
    private DatePicker selectorFecha;
    @FXML
    private TextArea commentTextArea;
    @FXML
    private ComboBox<String> cuentaComboBox;
    @FXML
    private ComboBox<String> MovimientoComboBox;
    @FXML
    private TextField cantidadText;
    @FXML
    private Button agregarTransaccionButton;
    @FXML
    private TableColumn<TransactionDetail, Integer> codigoColumna;
    @FXML
    private TableColumn<TransactionDetail, String> nombreColumna;
    @FXML
    private TableColumn<TransactionDetail, Double> debeColumna;
    @FXML
    private TableColumn<TransactionDetail, Double> haberColumna;
    @FXML
    private TableView<TransactionDetail> tablaRegistros;
    private AccountService ac;
    @FXML
    private Label saldoLabel;
    @FXML
    private Button eliminarCuentaBtn;
    @FXML
    private Button completarBtn;
    private TransaccionService service;    
    private Stage stage;
    private DateService servicioFecha;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        servicioFecha = new DateService();
        idText.setText(generarId());
        eliminarCuentaBtn.setDisable(true);
        MovimientoComboBox.getItems().addAll("Cargar","Abonar");
        //System.out.println("Debug 2");
        ac = new AccountService();
        cuentaComboBox.setItems(ac.getNombres());
        //System.out.println("Debug 3");
        codigoColumna.setCellValueFactory(new PropertyValueFactory<>("codigo"));
        nombreColumna.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        debeColumna.setCellValueFactory(new PropertyValueFactory<>("debe"));
        haberColumna.setCellValueFactory(new PropertyValueFactory<>("haber"));
        service = new TransaccionService();
        
    }    

    @FXML
    private void AgregarTransaccion(ActionEvent event) {
        //VALIDAR CAMPOS
        int codigo = 0;
        String nombre = "";
        double debe = 0.0;
        double haber = 0.0;
        if((this.cuentaComboBox==null)|| (this.cuentaComboBox.getSelectionModel().getSelectedItem().isEmpty() )){
            alerta("No se ha seleccionado cuenta a modificar");
        } else { //CuentaCombobox
                Account account = ac.buscarPorNombre(cuentaComboBox.getSelectionModel().getSelectedItem());
                if(this.MovimientoComboBox.getSelectionModel().getSelectedItem().isEmpty()||(this.MovimientoComboBox==null)){
                alerta("No se ha seleccionado el tipo de movimiento");

            }else{
                if(!(this.cantidadText.getText().matches("[0-9]+([.][0-9]+)?"))){
                    alerta("El campo cantidad contiene letras");                
                }else{    
                    if(MovimientoComboBox.getSelectionModel().getSelectedItem().equalsIgnoreCase(
                                        "cargar")){                    
                        debe = Double.parseDouble(cantidadText.getText());
                    }  else {
                        haber = Double.parseDouble(cantidadText.getText());
                    }
                    saldoLabel.setText(String.valueOf(account.getSaldo()));                   
                    TransactionDetail ts = new TransactionDetail(account.getCodigo(),account.getNombre(),debe,haber);                    
                    tablaRegistros.getItems().add(ts);
                }//Validacion cantidad
            }//Validacion movimiento combobox
        } //validacion cuenta combobox
    }

    @FXML
    private void eliminarRegistro(MouseEvent event) {
        eliminarCuentaBtn.setDisable(false);
    }

    @FXML
    private void eliminarDeLaTransaccion(ActionEvent event) {
        //TransactionDetail aux = tablaRegistros.getSelectionModel().getSelectedItem();
        ObservableList<TransactionDetail> seleccionado, lista;
        lista = tablaRegistros.getItems();
        seleccionado = tablaRegistros.getSelectionModel().getSelectedItems();
        
        
        Alert alerta = new Alert(Alert.AlertType.CONFIRMATION);
        alerta.setTitle("Confirmacion");
        alerta.setHeaderText("La cuenta será eliminada de la transacción");
        alerta.setContentText("¿Está seguro de realizar esta acción?");
        Optional<ButtonType> resultado = alerta.showAndWait(); 
        if(resultado.get() == ButtonType.OK){
            seleccionado.forEach(lista::remove);                    
        }
        
    }

    @FXML
    private void finalizarTransaccion(ActionEvent event) {
        if(validarFecha()){
          if(partidaDoble()){
            if(validarCampos()){
                //alerta("Congrats!");
                LocalDate ld = selectorFecha.getValue();
                String comentario = commentTextArea.getText();
                String id = idText.getText();
                Transaction t = new Transaction(id,comentario,ld);
                ObservableList<TransactionDetail> cuentas = tablaRegistros.getItems();
                System.out.println("Debuggin: "+cuentas);
                if(service.agregarTransaccion(t, cuentas)){
                    alerta("Transaccion realizada con exito");
                    stage.close();
                    
                }else{
                    alerta("Algo anduvo mal, la transaccion no se realizó");
                }


            } else {            
                alerta("Transaccion fallida");
                return;
            }
        } else {
            alerta("No se ha cumplido partida doble, verifique las cantidades");
        }
        
        }else{
            alerta("La fecha establecida no corresponde a la del periodo contable");
        }

    }
    
    private boolean validarCampos(){
        boolean f = true;
        if(selectorFecha.getValue()==null){
            alerta("La fecha se encuentra vacia");
            f = false;
            //return;
        } else {            
            if(commentTextArea.getText().isEmpty()){
                alerta("La descripcion del registro se encuentra vacio");
                f = false;
                //return;
            }
        }
        return f;
    }
    
    private void alerta(String content){        
                Alert alerta = new Alert(Alert.AlertType.INFORMATION);
                alerta.setHeaderText("Regristro de una nueva partida");
                alerta.setContentText(content);
            alerta.showAndWait();            
    }
        
    private String generarId(){
        String resul = "";
        LocalDateTime dateTime = LocalDateTime.now();
        int segundo = dateTime.getSecond();
        resul += segundo;
        int hora = dateTime.getHour();
        final String cadena  = "acegikmo0q1x23s456u7z89ABCDEFbydfhjlwtnpr";
        int n = cadena.length();
        Random r = new Random();
        for (int i=0;i<5;i++){
            resul += cadena.charAt(r.nextInt(n));
        }
        return resul+=hora;
    }
    
    private boolean partidaDoble(){
        boolean f = false;
        double debe=0.0;
        double haber = 0.0;
        ObservableList<TransactionDetail> list =
                tablaRegistros.getItems();
        for(TransactionDetail tc : list){
            debe = debe + debeColumna.getCellData(tc);
            haber = haber + haberColumna.getCellData(tc);
        }
        if(debe==haber){
            f = true;
        }
        return f;
    }
    
    public void setStage(Stage stage){
        this.stage = stage;
    }
    
    private boolean validarFecha(){
        boolean bandera = true;
        if(selectorFecha.getValue().isBefore(servicioFecha.getInicio())){
            bandera = false;
        } else {
            if(selectorFecha.getValue().isAfter(servicioFecha.getFin())){
                bandera = false;
            }
        }
        
        return bandera;
    }
}
