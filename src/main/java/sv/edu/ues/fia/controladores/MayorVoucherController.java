/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.controladores;

import java.net.URL;
import java.time.LocalDate;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import sv.edu.ues.fia.negocio.Mayor;

/**
 * FXML Controller class
 *
 * @author Kevin
 */
public class MayorVoucherController implements Initializable {
    @FXML
    private Text nombreText;
    @FXML
    private Text codigoText;
    @FXML
    private TableView<Mayor> tabla;
    @FXML
    private TableColumn<Mayor, LocalDate> fechaColumn;
    @FXML
    private TableColumn<Mayor, String> referenciaColumn;
    @FXML
    private TableColumn<Mayor,Double> debeColumn;
    @FXML
    private TableColumn<Mayor, Double> haberColumn;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        fechaColumn.setCellValueFactory(new PropertyValueFactory<>("fecha"));
        referenciaColumn.setCellValueFactory(new PropertyValueFactory<>("referencia"));
        debeColumn.setCellValueFactory(new PropertyValueFactory<>("debe"));
        haberColumn.setCellValueFactory(new PropertyValueFactory<>("haber"));
    }
    
    public void setNombre(String nombre){ nombreText.setText(nombre); }
    
    public void setCodigo(int codigo){ codigoText.setText(String.valueOf(codigo)); }
    
    public void setTableItems(ObservableList<Mayor> items){
        tabla.setItems(items);
    }
    
}
