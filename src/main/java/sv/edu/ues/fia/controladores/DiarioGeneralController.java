/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.controladores;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import sv.edu.ues.fia.negocio.DateService;
import sv.edu.ues.fia.negocio.TransaccionService;
import sv.edu.ues.fia.persistencia.Transaccion;

/**
 * FXML Controller class
 *
 * @author Kevin
 */
public class DiarioGeneralController implements Initializable {
    @FXML
    private VBox raiz;
    @FXML
    private Button agregarBtn;

    @FXML
    private ListView<VBox> transacciones;
    @FXML
    private Text periodo;
    private DateService service;
    private TransaccionService t;

    public VBox getRaiz() {
        return raiz;
    }

    public void setRaiz(VBox raiz) {
        this.raiz = raiz;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        service = new DateService();
        t = new TransaccionService();
        String texto = "";
        LocalDate inicio = service.getInicio();
        LocalDate fin = service.getFin();
        texto="De "+inicio.getDayOfMonth()+" de "+inicio.getMonth()+" al "+
                fin.getDayOfMonth()+" de "+fin.getMonth()+" del "+fin.getYear();
        periodo.setText(texto);
        try {
            
            inicializar(inicio,fin);           
            // TODO
        } catch (IOException ex) {
            Logger.getLogger(DiarioGeneralController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    

    @FXML
    private void agregarTransaccion(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(DiarioGeneralController.class.getResource("/fxml/NuevaTransaccion.fxml"));
        Parent raiz = loader.load();
        NuevaTransaccionController n = loader.getController();        
        Stage stage = new Stage();
        n.setStage(stage);
        Scene scene = new Scene(raiz);
        stage.setTitle("Agregar una nueva transaccion");
        stage.setScene(scene);
        stage.show();
    }
    
    private void inicializar(LocalDate inicio, LocalDate fin) throws IOException{
        List<Transaccion> lista = t.getTransacciones(inicio, fin);
        if(lista !=null){
            int i=0;
            for(Transaccion t : lista){
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(DiarioGeneralController.class.getResource("/fxml/Voucher.fxml"));
                VBox v = loader.load();
                final VoucherController vc= loader.getController();            

                vc.setTransaccion(t.getIdTransaccion());
                //FORMATEAR STRING
                vc.setFecha(formateoFecha(t.getFecha()));
                vc.setComentario(t.getDescripcion());
                if(i%2==0){
                    vc.setBackgroundColor("-fx-background-color: #DDFFCD;");
                } else {
                    vc.setBackgroundColor("-fx-background-color: #FFD8FD;");
                }           
                v.setPrefWidth(raiz.getPrefWidth()-40);
                transacciones.getItems().add(v);
                i++;
            }
        } else {
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(DiarioGeneralController.class.getResource("/fxml/Voucher.fxml"));
                VBox v = loader.load();
                final VoucherController vc= loader.getController();
                vc.setTransaccion("");
                //FORMATEAR STRING
                vc.setFecha("");
                vc.setComentario("");
                v.setPrefWidth(raiz.getPrefWidth()-40);
                transacciones.getItems().add(v);
        }
        
    }
    
    private String formateoFecha(Date d){
        return new SimpleDateFormat("dd-MMM-yyyy").format(d);        
    }
    

    
}
