/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.controladores;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import sv.edu.ues.fia.negocio.AccountService;
import sv.edu.ues.fia.negocio.OrdenCompra;
import sv.edu.ues.fia.servicios.ServicioKardexFrijoles;

/**
 * FXML Controller class
 *
 * @author Kevin
 */
public class RegistroComprasController implements Initializable {
    @FXML
    private Text cantidadExistencia;
    @FXML
    private TextField cantidadTextField;
    @FXML
    private RadioButton contadoRadio;
    @FXML
    private RadioButton creditoRadio;
    @FXML
    private DatePicker fecha;
    private OrdenCompra orden;
    @FXML
    private TextField precioUnitario;
    private AccountService ac;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        orden = new OrdenCompra();
        ac = new AccountService();
        cantidadExistencia.setText(String.valueOf(orden.getCantidadExistencias()));
        
    }    

    @FXML
    private void completarCompra(ActionEvent event) {
        double cantidad = Double.parseDouble(cantidadTextField.getText());
        double pU = Double.parseDouble(precioUnitario.getText());
        if(contadoRadio.isSelected() && creditoRadio.isSelected()){
            alerta("Se debe especificar un solo tipo de movimiento");
        }else{
            if(contadoRadio.isSelected()){
                ac.abonar(11101, cantidad*pU);
                ac.cargar(11301, cantidad*pU);
                orden.registroDeCompra(fecha.getValue(),cantidad, pU);
                alerta("Se ha registrado la compra con exito");
            } else {
                if(creditoRadio.isSelected()){
                    ac.abonar(21101, cantidad*pU);
                    ac.cargar(11301, cantidad*pU);
                    orden.registroDeCompra(fecha.getValue(), cantidad, pU);
                    alerta("Se ha registrado la compra con exito");
                }else{
                    alerta("No se ha seleccinado ningun movimiento");
                }
            
            }
        }
        
    }
    
        private void alerta(String content){        
                Alert alerta = new Alert(Alert.AlertType.INFORMATION);
                alerta.setHeaderText("Regristro de compra de materia prima");
                alerta.setContentText(content);
            alerta.showAndWait();            
    }
    
}
