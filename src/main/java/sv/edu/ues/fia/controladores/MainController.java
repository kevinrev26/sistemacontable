/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.controladores;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import sv.edu.ues.fia.negocio.CierreContable;
import sv.edu.ues.fia.negocio.EstadoFinanciero;
import sv.edu.ues.fia.negocio.KardexCollections;

/**
 * FXML Controller class
 *
 * @author Kevin
 */
public class MainController implements Initializable {
    @FXML
    private MenuItem cerrar;
    @FXML
    private MenuItem darioGeneral;
    @FXML
    private MenuItem mayorGeneral;
    @FXML
    private MenuItem usuarios;
    @FXML
    private MenuItem catalogo;
    @FXML
    private MenuItem acercaDe;
    @FXML
    private StackPane contenedor;
    
    private Stage stage;
    private Scene scene;
    private EstadoFinanciero estado;

    

    
    private DiarioGeneralController controllerDiario;
    @FXML
    private MenuItem estadoCapital;
    @FXML
    private MenuItem estadoResultados;
    @FXML
    private MenuItem estadoSituacionFinanciera;
    @FXML
    private MenuItem cierreContable;
    


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        //stage.getIcons().add(new Image("/img/icono_principal.jpg"));
        estado = new EstadoFinanciero();
        estadoResultados.setDisable(true);
        estadoCapital.setDisable(true);
        estadoSituacionFinanciera.setDisable(true);
        cierreContable.setDisable(true);
    }
    
    public void setStage(Stage stage){
        this.stage = stage;
    }
    
    public Scene getScene() {
        return scene;
    }
    
    public void setScene(Scene scene) {
        this.scene = scene;
    }
    
    @FXML
    void cerrar(ActionEvent event) {
           stage.close();
    }

    @FXML
    private void mostrarDiarioGeneral(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(this.getClass().getResource("/fxml/DiarioGeneral.fxml"));
        Parent raiz = loader.load();
        this.contenedor.getChildren().clear();
       
        this.contenedor.getChildren().add(raiz);
        
    }

    @FXML
    private void gestionarUsuarios(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(this.getClass().getResource("/fxml/Usuarios.fxml"));
        Parent raiz = loader.load();
        this.contenedor.getChildren().clear();
        this.contenedor.getChildren().add(raiz);
    }

    @FXML
    private void mostrarPlanDeCuentas(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(this.getClass().getResource("/fxml/PlanDeCuentas.fxml"));
        Parent raiz = loader.load();
        this.contenedor.getChildren().clear();
        this.contenedor.getChildren().add(raiz);        
    }

    @FXML
    private void seleccionarPeriodo(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(this.getClass().getResource("/fxml/Fecha.fxml"));
        Stage stage = new Stage();
        stage.setTitle("Seleccionar período contable");
        Parent raiz = loader.load();
        Scene scene = new Scene(raiz);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    private void crearBalanzaDeComprobacion(ActionEvent event) throws IOException {
        estadoResultados.setDisable(false);
        estado.crearBalanzaDeComprobacion();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(this.getClass().getResource("/fxml/BalanzaDeComprobacion.fxml"));
        Parent raiz = loader.load();
        contenedor.getChildren().clear();
        contenedor.getChildren().add(raiz);
    }

    @FXML
    private void estadoResultados(ActionEvent event) throws IOException {
        estadoCapital.setDisable(false);
        cierreContable.setDisable(false);
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(this.getClass().getResource("/fxml/EstadoResultados.fxml"));
        Parent raiz  = loader.load();
        contenedor.getChildren().clear();
        contenedor.getChildren().add(raiz);
    }

    @FXML
    private void mostrarMayorGeneral(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(this.getClass().getResource("/fxml/Mayor.fxml"));
        Parent raiz = loader.load();
        this.contenedor.getChildren().clear();
       
        this.contenedor.getChildren().add(raiz);
        
    }

    @FXML
    private void mostrarEstadoCapital(ActionEvent event) throws IOException {
        estadoSituacionFinanciera.setDisable(false);
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(this.getClass().getResource("/fxml/EstadoCapital.fxml"));
        Parent raiz = loader.load();
        this.contenedor.getChildren().clear();
       
        this.contenedor.getChildren().add(raiz);
    }

    @FXML
    private void mostrarBalanceGeneral(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(this.getClass().getResource("/fxml/EstadoSituacionFinanciera.fxml"));
        Parent raiz = loader.load();
        this.contenedor.getChildren().clear();
       
        this.contenedor.getChildren().add(raiz);
    }

    @FXML
    private void cerrarPeriodoContable(ActionEvent event) {
        CierreContable cierre = new CierreContable();
        cierre.cerrarPeriodoContable();
    }

    @FXML
    private void realizarCompra(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(this.getClass().getResource("/fxml/RegistroCompras.fxml"));
        Stage stage = new Stage();
        stage.setTitle("Realizar compra de materia prima");
        Parent raiz = loader.load();
        Scene scene = new Scene(raiz);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    private void mostrarLibroKardexFrijoles(ActionEvent event) throws IOException {
        KardexCollections collections = new KardexCollections();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(this.getClass().getResource("/fxml/Kardex.fxml"));
        Parent raiz = loader.load();
        KardexController controller = loader.getController();
        controller.setEncabezado("Control de existencias, libras de frijoles");
        controller.SetTable(collections.getKardexFrijoles());
        this.contenedor.getChildren().clear();
        this.contenedor.getChildren().add(raiz);  
    }

    @FXML
    private void mostrarAsignacion(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(this.getClass().getResource("/fxml/HojaCostos.fxml"));
        Stage stage = new Stage();
        stage.setTitle("Hoja de asignacion de costos");
        Parent raiz = loader.load();
        Scene scene = new Scene(raiz);
        stage.setScene(scene);
        stage.show();
    }
    
}
