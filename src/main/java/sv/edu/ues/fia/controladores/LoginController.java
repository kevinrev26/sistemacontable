/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.controladores;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javax.persistence.*;
import sv.edu.ues.fia.persistencia.Usuario;
import sv.edu.ues.fia.servicios.ServicioUsuario;

/**
 * FXML Controller class
 *
 * @author Kevin
 */
public class LoginController implements Initializable {
    @FXML
    private TextField nombreUsuario;
    @FXML
    private PasswordField contraseña;
    @FXML
    private Button inicarSesion;
    @FXML
    private Button salir;
    
    private Stage referenceStage;
    private Object AlerType;
    
    private EntityManagerFactory emf;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void iniciarSesionClicked(ActionEvent event) throws IOException {
        emf = Persistence.createEntityManagerFactory("base");
        
        ServicioUsuario servicio = new ServicioUsuario(emf.createEntityManager());
        
        Usuario aux = servicio.buscarUsuario(nombreUsuario.getText());
        
        
        if(aux!= null && 
                aux.getPassword().equals(contraseña.getText())){
            referenceStage.close();
            Stage stage = new Stage();
            stage.setTitle("Sistema contable - Los Nonualcos");
            stage.getIcons().add(new Image(this.getClass().getResourceAsStream("/img/icono_principal.jpg")));
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(LoginController.class.getResource("/fxml/Main.fxml"));
            Parent raiz = loader.load();
            MainController main = loader.getController();
            main.setStage(stage);
            Scene scene = new Scene(raiz);
            stage.setScene(scene);
            stage.show();        
        } else {
            Alert alerta = new Alert(AlertType.WARNING);
            alerta.setTitle("Credenciales erroneas");
            alerta.setHeaderText("Ingreso de usuario");
            alerta.setContentText("Las credenciales ingresadas son erroneas, intentelo de nuevo");
            alerta.showAndWait();
        
        }
    }

    @FXML
    private void exit(ActionEvent event) {
       referenceStage.close();
    }
    
    //Implementar en cada controlador
    public void setStage(Stage stage){
        referenceStage = stage;
    
    }
    
    
}
