/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.controladores;

import java.net.URL;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXML;
import javafx.scene.layout.VBox;
import javafx.fxml.Initializable;
import javafx.scene.text.Text;
import javafx.fxml.Initializable;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import sv.edu.ues.fia.negocio.DateService;
import sv.edu.ues.fia.persistencia.PlanDeCuentas;
import sv.edu.ues.fia.servicios.ServicioPlanDeCuentas;

/**
 * FXML Controller class
 *
 * @auth    @FXML
    private Text periodo;
    @FXML
    private VBox contenedorActivos;
    @FXML
    private VBox contenedorPasivos;
    @FXML
    private VBox contenedorCapital;
or Kevin
 */
public class EstadoSituacionFinancieraController implements Initializable{
    @FXML
    private Text periodo;
    @FXML
    private VBox contenedorActivos;
    @FXML
    private VBox contenedorPasivos;
    @FXML
    private VBox contenedorCapital;
    private DateService service;
    private ServicioPlanDeCuentas servicioCuentas;
    private double totalActivos;
    private double totalPasivos;
    private double totalCapital;
    @FXML
    private HBox contenedorRecursos;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        service=new DateService();
        servicioCuentas = new ServicioPlanDeCuentas(entity());
        periodo.setText(periodo());
        totalActivos = total(servicioCuentas.consultarList("1%"));
        totalPasivos = total(servicioCuentas.consultarList("2%"));
        totalCapital = total(servicioCuentas.consultarList("3%"));
        rellenar(contenedorActivos,servicioCuentas.consultarList("1%"),"Total de Activos: ",totalActivos);
        rellenar(contenedorPasivos,servicioCuentas.consultarList("2%"),"Total de Pasivos: ",totalPasivos);
        rellenar(contenedorCapital,servicioCuentas.consultarList("3%"),"Total de Activos: ",totalCapital);
        Text totales = new Text("Pasivos mas Capital: ");
        Text cantidad = new Text(String.valueOf(totalPasivos + totalCapital));
        contenedorRecursos.getChildren().addAll(totales,cantidad);
        
    }
    
    
    private String periodo(){
        LocalDate inicio = service.getInicio();
        LocalDate fin = service.getFin();      
        return "De "+inicio.getDayOfMonth()+" de "+inicio.getMonth()+" al "+
                fin.getDayOfMonth()+" de "+fin.getMonth()+" del "+fin.getYear();
    }
    
    private String formateo(double d){
        NumberFormat formateo = NumberFormat.getCurrencyInstance(Locale.US);
        return formateo.format(d);
    }
    
    private EntityManager entity(){
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("base");
        return emf.createEntityManager();
    }
    
    private double total(List<PlanDeCuentas> lista){
       
        double suma =  0.0;
        for(PlanDeCuentas c : lista){
            suma = suma + c.getSaldo();
        }
        
        return suma;
    }
    
    private void rellenar(VBox vbox, List<PlanDeCuentas> p, String texto, double cantidad){
        for(PlanDeCuentas c : p){
            HBox renglon = new HBox();
            Text nombre = new Text(c.getNombre());
            Text cant = new Text(formateo(c.getSaldo()));
            renglon.getChildren().addAll(nombre,cant);
            vbox.getChildren().add(renglon);
        }
        Text total = new Text(texto);
        Text cantidadTotal = new Text(formateo(cantidad));
        HBox r = new HBox();
        r.getChildren().addAll(total,cantidadTotal);
        vbox.getChildren().add(r);
    }
    
}
