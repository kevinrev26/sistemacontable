/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.controladores;

import java.net.URL;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import sv.edu.ues.fia.negocio.DateService;
import sv.edu.ues.fia.persistencia.PlanDeCuentas;
import sv.edu.ues.fia.servicios.ServicioPlanDeCuentas;

/**
 * FXML Controller class
 *
 * @author Kevin
 */
public class EstadoCapitalController implements Initializable {
    @FXML
    private Text periodo;
    @FXML
    private VBox inversiones;
    @FXML
    private VBox desinversiones;
    @FXML
    private Text totalCapitalContable;
    private double totalInversion;
    private double totalDesinversion;
    private double totalCapital;
    private DateService service;
    private ServicioPlanDeCuentas servicioCuentas;
           

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        service=new DateService();
        servicioCuentas = new ServicioPlanDeCuentas(entity());
        periodo.setText(periodo());
        List<PlanDeCuentas> cuentasCapital= servicioCuentas.consultarList("3%");
        List<PlanDeCuentas> inversion = getCuentasInversiones(cuentasCapital);
        List<PlanDeCuentas> desinversion = getCuentasDesinversiones(cuentasCapital);
        totalInversion = total(inversion);
        totalDesinversion = total(desinversion);
        totalCapital = totalInversion + totalDesinversion;
        rellenar(inversiones,inversion,"Total inversion: ",totalInversion);
        rellenar(desinversiones,desinversion,"Total desinversion: ",totalDesinversion);
        totalCapitalContable.setText(formateo(totalCapital));
        
        
    }    
    
    private String periodo(){
        LocalDate inicio = service.getInicio();
        LocalDate fin = service.getFin();      
        return "De "+inicio.getDayOfMonth()+" de "+inicio.getMonth()+" al "+
                fin.getDayOfMonth()+" de "+fin.getMonth()+" del "+fin.getYear();
    }                                                                                                                             
    
    private String formateo(double d){
        NumberFormat formateo = NumberFormat.getCurrencyInstance(Locale.US);
        return formateo.format(d);
    }
    
    private EntityManager entity(){
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("base");
        return emf.createEntityManager();
    }
    
    private ArrayList<PlanDeCuentas> getCuentasInversiones(List<PlanDeCuentas> p){
        ArrayList<PlanDeCuentas> i = new ArrayList<>();;
        for(PlanDeCuentas c : p){
            if(c.getSaldo()>=0){ i.add(c); }
        }
        
        return i;
    }
    
        private ArrayList<PlanDeCuentas> getCuentasDesinversiones(List<PlanDeCuentas> p){
        ArrayList<PlanDeCuentas> d = new ArrayList<>();
        for(PlanDeCuentas c : p){
            if(0>c.getSaldo()){ d.add(c); }
        }
        
        return d;
    }
    
        
    private double total(List<PlanDeCuentas> lista){
       
        double suma =  0.0;
        for(PlanDeCuentas c : lista){
            suma = suma + c.getSaldo();
        }
        
        return suma;
    }
    
    private void rellenar(VBox vbox, List<PlanDeCuentas> p, String texto, double cantidad){
        for(PlanDeCuentas c : p){
            HBox renglon = new HBox();
            Text nombre = new Text(c.getNombre());
            Text cant = new Text(formateo(c.getSaldo()));
            renglon.getChildren().addAll(nombre,cant);
            vbox.getChildren().add(renglon);
        }
        Text total = new Text(texto);
        Text cantidadTotal = new Text(formateo(cantidad));
        HBox r = new HBox();
        r.getChildren().addAll(total,cantidadTotal);
        vbox.getChildren().add(r);
    }
}
