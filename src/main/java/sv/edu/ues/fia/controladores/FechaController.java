/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.controladores;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.DatePicker;
import sv.edu.ues.fia.negocio.DateService;

/**
 * FXML Controller class
 *
 * @author Kevin
 */
public class FechaController implements Initializable {
    @FXML
    private DatePicker inicio;
    @FXML
    private DatePicker fin;
    private DateService service;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        service = new DateService();
        inicio.setValue(service.getInicio());
        fin.setValue(service.getFin());
       
        
    }    

    @FXML
    private void actualizar(ActionEvent event) {
        if(inicio.getValue()==null && fin.getValue()==null){
            alerta("Las fechas estan vacias, verifique");
        }else{
            if(inicio.getValue()==null){
                alerta("La fecha de inicio se encuentra vacia");
            } else {
                if(fin.getValue()==null){
                    alerta("La fecha de fin se encuentra vacia");
                } else {
                    LocalDate i = inicio.getValue();
                    LocalDate f = fin.getValue();
                    if(i.isAfter(f)){
                        alerta("La fecha de inicio es mas tardia que la de fin");
                    } else {
                        service.actualizarPeriodo(i, f);
                        inicio.setValue(service.getInicio());
                        fin.setValue(service.getFin());
                        alerta("Persistencia es correcta");
                    }//Rango de fechas
                }//fin
            }//inicio
        }//Ambas fechas
    }
    
    private void alerta(String content){
        Alert alerta = new Alert(AlertType.INFORMATION);
        alerta.setHeaderText("Modificacion del periodo contable");
        alerta.setContentText(content);
        alerta.showAndWait();
    }
    
}
