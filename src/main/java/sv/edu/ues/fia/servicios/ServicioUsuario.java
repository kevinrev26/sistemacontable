/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.servicios;

import java.util.List;
import javax.persistence.*;
//import javafx.collections.ObservableList;
import sv.edu.ues.fia.persistencia.Usuario;

/**
 *
 * @author Kevin
 */
public class ServicioUsuario {
   
    public EntityManager em;

    public ServicioUsuario(EntityManager em) {
        this.em = em;
        
    }
    
    public Usuario crearUsuario(String username, String password, String nombreCompleto){
          Usuario usu = new Usuario(username, password, nombreCompleto);
          em.persist(usu);
          return usu;
    }
    
    public void eliminarUsuario(String username){
        Usuario usu = buscarUsuario(username);
        if(usu !=null){
            em.remove(usu);
        }
    }
    
    public Usuario buscarUsuario(String username){
        return em.find(Usuario.class, username);
    }
    
    public  List<Usuario> getTodosUsuarios(){
        TypedQuery<Usuario> queary = em.createQuery(
                                        "Select u FROM Usuario u", Usuario.class);
        return queary.getResultList();
    }
}
