/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.servicios;

import java.util.List;
import javax.persistence.*;
import sv.edu.ues.fia.persistencia.PlanDeCuentas;

/**
 *
 * @author Kevin
 */
public class ServicioPlanDeCuentas {
    
    public EntityManager em2;
    
    public ServicioPlanDeCuentas(EntityManager em){
        this.em2 = em;
    }
    
    public PlanDeCuentas crearCuenta(int codigo, String nombre, boolean naturaleza,
            double saldo){
        PlanDeCuentas cuenta = new PlanDeCuentas(codigo, nombre, naturaleza, saldo);
        em2.getTransaction().begin();
        em2.persist(cuenta);
        em2.getTransaction().commit();
        em2.clear();
        return cuenta;
    }
    
    public void eliminarCuenta(int codigo){
        PlanDeCuentas cuenta = this.buscarCuenta(codigo);
        if(cuenta!=null){
            em2.getTransaction().begin();
            em2.remove(cuenta);
            em2.getTransaction().commit();
            
        }
        em2.clear();
    }
    
    public PlanDeCuentas buscarCuenta(int codigo){
        return em2.find(PlanDeCuentas.class,codigo);
    }
    
    public List<PlanDeCuentas> getCuentas(){
            TypedQuery<PlanDeCuentas> queary = em2.createQuery(
                                        "Select p FROM PlanDeCuentas p ORDER BY p.codigo ASC",
                                        PlanDeCuentas.class);
        return queary.getResultList();
    }
    
    public void actualizar(PlanDeCuentas cuenta){
        PlanDeCuentas c = buscarCuenta(cuenta.getCodigo());
        if(c!=null){
            em2.getTransaction().begin();
            c.setCodigo(cuenta.getCodigo());
            c.setNombre(cuenta.getNombre());
            c.setNaturaleza(cuenta.getNaturaleza());
            c.setSaldo(cuenta.getSaldo());
            em2.getTransaction().commit();
        }
        em2.clear();
    }
    
    public PlanDeCuentas buscarPorNombre(String nombre){
        List<PlanDeCuentas> resultado =  em2.createNamedQuery("PlanDeCuentas.findByNombre").setParameter("nombre", nombre).getResultList();
        //System.out.println("Especificacion"+ resultado.get(0).getCodigo());
        return resultado.get(0);
    }
    
    public void cargar(int codigo, Double cantidad){
        PlanDeCuentas c = buscarCuenta(codigo);
        em2.getTransaction().begin();
        if(c.getNaturaleza()){
            c.setSaldo(c.getSaldo()-cantidad);
        } else {
            c.setSaldo(c.getSaldo()+cantidad);
        }
        em2.getTransaction().commit();
        em2.clear();
    }
    
    public void abonar(int codigo, Double cantidad){
        PlanDeCuentas c = buscarCuenta(codigo);
        em2.getTransaction().begin();
        if(c.getNaturaleza()){
            c.setSaldo(c.getSaldo()+cantidad);
        } else {
            c.setSaldo(c.getSaldo()-cantidad);
        }
        em2.getTransaction().commit();
        em2.clear();
    }
    
    public PlanDeCuentas consultar(String parametro){
        return (PlanDeCuentas)em2.createNamedQuery("PlanDeCuentas.consultar").setParameter("param",parametro).getSingleResult();
    }
    
    public List<PlanDeCuentas> consultarList(String parametro){
        return em2.createNamedQuery("PlanDeCuentas.consultar").setParameter("param",parametro).getResultList();
    }
    
    public void grabarUtilidad(double utilidad){
        PlanDeCuentas c = buscarCuenta(33102);
        em2.getTransaction().begin();
        c.setSaldo(utilidad);
        em2.getTransaction().commit();
        em2.clear();
    
    }
    
    public void actualizarSaldo(int codigo,double saldo){
        PlanDeCuentas c = buscarCuenta(codigo);
        em2.getTransaction().begin();
        c.setSaldo(saldo);
        em2.getTransaction().commit();
        em2.clear();
    }
}
