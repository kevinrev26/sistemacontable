/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.servicios;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import sv.edu.ues.fia.persistencia.BalanzaComprobacion;
import sv.edu.ues.fia.persistencia.PlanDeCuentas;

/**
 *
 * @author Kevin
 */
public class ServicioBalanza {
    private final EntityManager em;
    
    public ServicioBalanza(EntityManager em){
        this.em = em;
        
    }
    
    public void agregarRegistro(PlanDeCuentas c){
        BalanzaComprobacion balanza = new BalanzaComprobacion();
        em.getTransaction().begin();
        balanza.setCodigo(c.getCodigo());
        balanza.setNombre(c.getNombre());
        if(c.getNaturaleza()){
            balanza.setDebe(0.0);
            balanza.setHaber(c.getSaldo());
        }else{
            balanza.setDebe(c.getSaldo());
            balanza.setHaber(0.0);
        }
        em.persist(balanza);
        em.getTransaction().commit();
        em.clear();
    }
    
    public void actualizarBalanza(List<PlanDeCuentas> list){
        for(PlanDeCuentas c: list){
            BalanzaComprobacion b = buscarPorNombre(c);
            
            if(b!=null){
                em.getTransaction().begin();
                if(c.getNaturaleza()){
                    b.setDebe(0.0);
                    b.setHaber(c.getSaldo());
                }else{
                    b.setDebe(c.getSaldo());
                    b.setHaber(0.0);
                }
                em.getTransaction().commit();
            }else{
                agregarRegistro(c);
            }
        }
        em.clear();
    }
    
    public BalanzaComprobacion buscarPorNombre(PlanDeCuentas c){
        try{
        return (BalanzaComprobacion)em.createNamedQuery("BalanzaComprobacion.findByCodigo",BalanzaComprobacion.class).setParameter("codigo", c.getCodigo()).getSingleResult();
        } catch(NoResultException e){
            return null;
        }
    }
    
    public List<BalanzaComprobacion> recuperarRegistros(){
        return em.createNamedQuery("BalanzaComprobacion.findAll",BalanzaComprobacion.class).getResultList();
    }
    
    public double sumaDebe(){
        double suma =0.0;
        List<BalanzaComprobacion> list = recuperarRegistros();
        for(BalanzaComprobacion b : list){
            suma = suma + b.getDebe();        
        }
        return suma;
    }
    
    public double sumaHaber(){
        double suma =0.0;
        List<BalanzaComprobacion> list = recuperarRegistros();
        for(BalanzaComprobacion b : list){
            suma = suma + b.getHaber();        
        }
        return suma;
    }
        
}
