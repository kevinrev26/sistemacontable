/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.servicios;

import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import sv.edu.ues.fia.negocio.Mayor;
import sv.edu.ues.fia.persistencia.DetalleTransaccion;
import sv.edu.ues.fia.persistencia.Transaccion;

/**
 *
 * @author Kevin
 */
public class ServicioTransaccion {
    public EntityManager em;

    public ServicioTransaccion(EntityManager em) {
        this.em = em;
    }
    
    public Transaccion  crearTransaccion(String id, String commentario, Date fecha, List<DetalleTransaccion> lista){
        //boolean f = true;
        Transaccion transaccion = new Transaccion();
        em.getTransaction().begin();
        transaccion.setIdTransaccion(id);
        transaccion.setDescripcion(commentario);
        transaccion.setFecha(fecha);
        for(DetalleTransaccion d : lista){
            d.setIdTransaccion(transaccion);
        }
        transaccion.setDetalleTransaccionList(lista);
        em.persist(transaccion);
        for(DetalleTransaccion d : lista){
            em.persist(d);
        }        
        em.getTransaction().commit();
        em.clear();
        return transaccion;
    }
    
    public List<Transaccion> transaccionDiario(Date inicio, Date fin){
        return em.createNamedQuery("Transaccion.buscarEntreFecha", Transaccion.class).setParameter("inicio",inicio).setParameter("fin",fin).getResultList();
    }
    
    public List<Mayor> getCuentasMayor(int codigo, Date inicio, Date fin){
        Query query = em.createQuery("SELECT NEW sv.edu.ues.fia.negocio.Mayor(t.fecha, t.idTransaccion,d.debe,d.haber) FROM  DetalleTransaccion d, Transaccion t WHERE d.codigo = :codigo  AND t.fecha BETWEEN :inicio AND :fin", Mayor.class);
        return query.setParameter("codigo",codigo).setParameter("inicio",inicio).setParameter("fin", fin).getResultList();
    }
}
