/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.servicios;

import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import sv.edu.ues.fia.persistencia.PeriodoContable;

/**
 *
 * @author Kevin
 */
public class ServicioFecha {
    public EntityManager em;

    public ServicioFecha(EntityManager em) {
        this.em = em;
    }
    
    public List<PeriodoContable> getPeriodo(){
        TypedQuery<PeriodoContable> query = 
                                        em.createQuery(
                                                "Select p FROM PeriodoContable p",
                                                        PeriodoContable.class);
        
        return query.getResultList();
    }
    
    public void actualizar(PeriodoContable pc){
        PeriodoContable p = getPeriodo().get(0);        
        if(p!=null){
            
            em.getTransaction().begin();
            p.setInicio(pc.getInicio());
            p.setFin(pc.getFin());
            em.getTransaction().commit();        
        }
        
        em.clear();
    }
    
    public PeriodoContable buscarPeriodo(Date inicio){
        return em.find(PeriodoContable.class,inicio);
    }
}
