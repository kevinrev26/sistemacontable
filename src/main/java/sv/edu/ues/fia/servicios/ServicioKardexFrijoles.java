/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.ues.fia.servicios;

import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import sv.edu.ues.fia.persistencia.ColaPrecios;
import sv.edu.ues.fia.persistencia.KardexFrijoles;

/**
 *
 * @author Kevin
 */
public class ServicioKardexFrijoles {
    public EntityManager em;
    
    public ServicioKardexFrijoles(EntityManager em){
        this.em = em;
    }
    
    public void registrarCompra(Date fecha, double cantidad, double precioUnitario){
        //double cantidadExistencia=0.0;
        double cantidadExistencia = getCantidadEntrada() - getCantidadSalida();
        double montoTotalExistencia = getMontoEntrada() - getMontoSalida();  
        KardexFrijoles k = new KardexFrijoles();
        em.getTransaction().begin();
        k.setCantidadEntrada(cantidad);
        k.setFecha(fecha);
        k.setCostoUnitarioEntrada(precioUnitario);
        k.setMontoTotalEntrada(precioUnitario*cantidad);
        k.setCantidadSalida(0.0);
        k.setCostoUnitarioSalida(0.0);
        k.setMontoTotalSalida(0.0);
        k.setCantidadExistencia(cantidadExistencia);
        k.setMontoTotalExistencia(montoTotalExistencia);
        em.persist(k);
        em.getTransaction().commit();
        em.clear();
        
    }
    
    public void registrarSalida(Date fecha, double cantidad, double precioUnitario){
        double cantidadExistencia = getCantidadEntrada() - getCantidadSalida();
        double montoTotalExistencia = getMontoEntrada() - getMontoSalida();  
        KardexFrijoles k = new KardexFrijoles();
        em.getTransaction().begin();
        k.setCantidadSalida(cantidad);
        k.setFecha(fecha);
        k.setCostoUnitarioSalida(precioUnitario);
        k.setMontoTotalSalida(precioUnitario*cantidad);
        k.setCantidadEntrada(0.0);
        k.setCostoUnitarioEntrada(0.0);
        k.setMontoTotalEntrada(0.0);
        k.setCantidadExistencia(cantidadExistencia);
        k.setMontoTotalExistencia(montoTotalExistencia);
        em.persist(k);
        em.getTransaction().commit();
        em.clear();
        
    
    }
    
    public double getCantidadEntrada(){
         List<KardexFrijoles> l = getRegistros();
         double sum = 0.0;
         for(KardexFrijoles k : l){
             sum = sum + k.getCantidadEntrada();
         }
         
         return sum;
        
    }
    
    public double getCantidadSalida(){
         List<KardexFrijoles> l = getRegistros();
         double sum = 0.0;
         for(KardexFrijoles k : l){
             sum = sum + k.getCantidadSalida();
         }
         
         return sum;
        
    }
    
    public double getMontoEntrada(){
         List<KardexFrijoles> l = getRegistros();
         double sum = 0.0;
         for(KardexFrijoles k : l){
             sum = sum + k.getMontoTotalEntrada();
         }
         
         return sum;
        
    }
    
    public double getMontoSalida(){
         List<KardexFrijoles> l = getRegistros();
         double sum = 0.0;
         for(KardexFrijoles k : l){
             sum = sum + k.getMontoTotalSalida();
         }
         
         return sum;
        
    }
    
    public List<KardexFrijoles> getRegistros(){
        return em.createNamedQuery("KardexFrijoles.findAll", KardexFrijoles.class).getResultList();
    }
    
    public void actualizarPrecios(){
        List<KardexFrijoles> lista = getRegistros();
        
        for(KardexFrijoles k : lista){
            ColaPrecios cola = em.find(ColaPrecios.class, k.getId());
            
            if(cola == null){
                em.getTransaction().begin();
                cola = new ColaPrecios(k.getId(),k.getCantidadEntrada(),k.getCostoUnitarioEntrada());
                em.persist(cola);
                em.getTransaction().commit();
            }
        }
        em.clear();
    }
    
    public List<KardexFrijoles> getRegistrosEnElPeriodo(Date inicio, Date fin){
        return em.createNamedQuery("KardexFrijoles.findByFechas",KardexFrijoles.class).setParameter("inicio",inicio).setParameter("fin",fin).getResultList();
    }
    
    public void actualizarPostSalida(int codigo, double cantidad){
        ColaPrecios cola = em.find(ColaPrecios.class,codigo);
        em.getTransaction().begin();
        cola.setCantidad(cantidad);
        em.persist(cola);
        em.getTransaction().commit();
        em.clear();
    }
    
    public List<ColaPrecios> getPrecios(){
        return em.createNamedQuery("ColaPrecios.findAll",ColaPrecios.class).getResultList();
    }
}
