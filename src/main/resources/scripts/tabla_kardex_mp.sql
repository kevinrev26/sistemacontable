﻿-- Table: kardex_frijoles

-- DROP TABLE kardex_frijoles;

CREATE TABLE kardex_frijoles
(
  cantidad_entrada double precision NOT NULL,
  costo_unitario_entrada double precision NOT NULL,
  monto_total_entrada double precision NOT NULL,
  cantidad_salida double precision NOT NULL,
  id serial NOT NULL,
  costo_unitario_salida double precision NOT NULL,
  monto_total_salida double precision NOT NULL,
  cantidad_existencia double precision NOT NULL,
  monto_total_existencia double precision NOT NULL,
  fecha date,
  CONSTRAINT identificacion_kardex PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE kardex_frijoles
  OWNER TO contador;
