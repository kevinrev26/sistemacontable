
CREATE TABLE periodo_contable
(
  ID     SERIAL        NOT NULL,
  inicio date NOT NULL, -- Fecha de inicio del periodo contable
  fin date NOT NULL,
  CONSTRAINT indentificador PRIMARY KEY (ID)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE periodo_contable
  OWNER TO contador;
COMMENT ON COLUMN periodo_contable.inicio IS 'Fecha de inicio del periodo contable';

INSERT INTO periodo_contable(inicio,fin) VALUES
	('2015-11-01','2015-11-30');