﻿-- Table: cola_precios

-- DROP TABLE cola_precios;

CREATE TABLE cola_precios
(
  id integer NOT NULL,
  cantidad double precision NOT NULL,
  precio_unitario double precision NOT NULL,
  CONSTRAINT "identificador del precio" PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cola_precios
  OWNER TO contador;
