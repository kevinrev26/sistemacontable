create table balanza_comprobacion(
    ID    serial    not null,
    codigo int4     not null,
    nombre VARCHAR(255)         not null,
    debe double precision           null,
    haber double precision          null,
    CONSTRAINT identificador PRIMARY KEY (ID)
);
