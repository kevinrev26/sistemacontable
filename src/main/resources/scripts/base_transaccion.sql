﻿/*==============================================================*/
/* DBMS name:      PostgreSQL 8                                 */
/* Created on:     21/11/2015 10:35:42 a.m.                     */
/*==============================================================*/


drop index REGISTRA_FK;

drop index DETALLE_TRANSACCION_PK;

drop table DETALLE_TRANSACCION;

drop index TRANSACCION_PK;

drop table TRANSACCION;

/*==============================================================*/
/* Table: DETALLE_TRANSACCION                                   */
/*==============================================================*/
create table DETALLE_TRANSACCION (
   ID                   SERIAL               not null,
   CODIGO               INT4                 not null,
   ID_TRANSACCION       VARCHAR(255)         not null,
   NOMBRE               VARCHAR(255)         null,
   DEBE                 double precision               null,
   HABER                double precision               null,
   constraint PK_DETALLE_TRANSACCION primary key (ID)
);

/*==============================================================*/
/* Index: DETALLE_TRANSACCION_PK                                */
/*==============================================================*/
create unique index DETALLE_TRANSACCION_PK on DETALLE_TRANSACCION (
ID
);

/*==============================================================*/
/* Index: REGISTRA_FK                                           */
/*==============================================================*/
create  index REGISTRA_FK on DETALLE_TRANSACCION (
ID_TRANSACCION
);

/*==============================================================*/
/* Table: TRANSACCION                                           */
/*==============================================================*/
create table TRANSACCION (
   ID_TRANSACCION       VARCHAR(255)         not null,
   DESCRIPCION          VARCHAR(255)         null,
   FECHA                DATE                 null,
   constraint PK_TRANSACCION primary key (ID_TRANSACCION)
);

/*==============================================================*/
/* Index: TRANSACCION_PK                                        */
/*==============================================================*/
create unique index TRANSACCION_PK on TRANSACCION (
ID_TRANSACCION
);

alter table DETALLE_TRANSACCION
   add constraint FK_DETALLE__REGISTRA_TRANSACC foreign key (ID_TRANSACCION)
      references TRANSACCION (ID_TRANSACCION)
      on delete restrict on update restrict;

