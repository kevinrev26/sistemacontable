﻿-- Table: plan_de_cuentas

-- DROP TABLE plan_de_cuentas;

CREATE TABLE plan_de_cuentas
(
  codigo integer NOT NULL, -- Guardar el codigo de la cuenta
  nombre character varying(255) NOT NULL,
  naturaleza boolean NOT NULL,
  saldo double precision NOT NULL DEFAULT 0,
  CONSTRAINT plan_de_cuentas_pkey PRIMARY KEY (codigo)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE plan_de_cuentas
  OWNER TO contador;
COMMENT ON COLUMN plan_de_cuentas.codigo IS 'Guardar el codigo de la cuenta';

